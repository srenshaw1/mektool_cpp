/*
File: 'timer.cpp'
Author: Seth Renshaw
Date Created: 7/16/2016
*/

#pragma once

#include "SDL_helper.h"
#include "types.h"

struct Timer {
  float last_time = 0;
  float cur_time = 0;
  float accumulator = 0;

  void update() {
    cur_time = RuntimeInSeconds();
    accumulator += cur_time - last_time;
    last_time = cur_time;
  }

  static
  r32 RuntimeInSeconds() {
    return SDL_GetTicks() * 0.001f;
  }
};