/*
File: 'text_table.h'
Author: Seth Renshaw
Date Created: 7/12/2016
*/

#pragma once

#include "renderer.h"
#include "database.h"

#include "types.h"
#include "SDL_helper.h"

struct Text {
  String main;
  String extra;
  b32 single;
  SDL_Texture* main_texture;
  SDL_Texture* extra_texture;

  Text(char const* a)
  : main(a), extra(), single(true)
  , main_texture(nullptr), extra_texture(nullptr) {}

  Text(char const* a, char const* b)
  : main(a), extra(b), single(false)
  , main_texture(nullptr), extra_texture(nullptr) {
    if (extra.size == 0) {
      single = true;
    }
  }

  Text(Text const& t)
  : main(t.main), extra(t.extra), single(t.single)
  , main_texture(nullptr), extra_texture(nullptr) {
  }

  b32 loadTexture(Renderer* renderer) {
    SDL_Color white = { 255, 255, 255, 255 };
    main_texture = renderer->createTextTexture(main.text, white);
    if (!main_texture) {
      return false;
    }

    if (!single) {
      extra_texture = renderer->createTextTexture(extra.text, white);
      if (!extra_texture) {
        SDL_DestroyTexture(main_texture);
        main_texture = nullptr;
        return false;
      }
    }

    return true;
  }

  SDL_Point textSize(b32 is_vert) {
    if (!main_texture) {
      assert(!extra_texture);
      return {};
    }

    SDL_Point main_size;
    SDL_QueryTexture(main_texture, nullptr, nullptr, &main_size.x, &main_size.y);
    if (single) {
      return main_size;
    }

    SDL_Point extra_size;
    SDL_QueryTexture(extra_texture, nullptr, nullptr, &extra_size.x, &extra_size.y);

    SDL_Point size = {};
    if (is_vert) {
      size.x = (main_size.x > extra_size.x) ? main_size.x : extra_size.x;
      size.y = main_size.y + extra_size.y;
    } else {
      size.x = main_size.x + extra_size.x;
      size.y = (main_size.y > extra_size.y) ? main_size.y : extra_size.y;
    }
    return size;
  }

  r32 textRatio() {
    if (!main_texture || !extra_texture) {
      return 0.0f;
    }

    s32 main_w;
    SDL_QueryTexture(main_texture, nullptr, nullptr, &main_w, nullptr);
    s32 extra_w;
    SDL_QueryTexture(extra_texture, nullptr, nullptr, &extra_w, nullptr);
    return (r32)main_w / (r32)(main_w + extra_w);
  }

  ~Text() {
    if (main_texture) {
      SDL_DestroyTexture(main_texture);
    }
    if (extra_texture) {
      SDL_DestroyTexture(extra_texture);
    }
  }
};

#define MEKTON_TYPES(ENTRY) \
  ENTRY(Aquamecha) \
  ENTRY(Beast) \
  ENTRY(Fighter) \
  ENTRY(Hybrid) \

#define POWERPLANT_TYPE(ENTRY) \
  ENTRY(Cool) \
  ENTRY(Hot) \

#define SERVO_LIST(ENTRY) \
  ENTRY(Arm) \
  ENTRY(Head) \
  ENTRY(Leg) \
  ENTRY(Pod) \
  ENTRY(Tail) \
  ENTRY(Torso) \
  ENTRY(Wing) \

#define TEXT_ONE_VALUE_LIST(ENTRY) \
  ENTRY(Armor) \
  ENTRY(Clear) \
  ENTRY(Closed) \
  ENTRY(Cockpit) \
  ENTRY(Cost) \
  ENTRY(Crew) \
  ENTRY(Equipment) \
  ENTRY(Exit) \
  ENTRY(Export) \
  ENTRY(Form) \
  ENTRY(GES) \
  ENTRY(Grade) \
  ENTRY(Kills) \
  ENTRY(Load) \
  ENTRY(MA) \
  ENTRY(MV) \
  ENTRY(Name) \
  ENTRY(Open) \
  ENTRY(Option) \
  ENTRY(Powerplant) \
  ENTRY(Save) \
  ENTRY(Servo) \
  ENTRY(SP) \
  ENTRY(Spaces) \
  ENTRY(Thrusters) \
  ENTRY(Transformation) \
  ENTRY(Tread) \
  ENTRY(Treads) \
  ENTRY(Type) \
  ENTRY(Weight) \
  ENTRY(Wheel) \
  ENTRY(Wheels) \

#define TEXT_TWO_VALUE_LIST(ENTRY) \
  ENTRY(Add,Equipment) \
  ENTRY(Add,Servo) \
  ENTRY(Cockpit,Info) \
  ENTRY(Ejection,Seat) \
  ENTRY(Environment,Seat) \
  ENTRY(Equipment,List) \
  ENTRY(GES,MA) \
  ENTRY(Link,Info) \
  ENTRY(Mecha,Info) \
  ENTRY(Movement,Systems) \
  ENTRY(Option,Info) \
  ENTRY(Remove,Equipment) \
  ENTRY(Remove,Servo) \
  ENTRY(Servo,List) \
  ENTRY(Servo,Panel) \
  ENTRY(Thruster,MA) \
  ENTRY(Weapon,Links) \

#define GRADE_LIST(ENTRY) \
  ENTRY(Super,Light) \
  ENTRY(Light,Weight) \
  ENTRY(Striker,) \
  ENTRY(Medium,Striker) \
  ENTRY(Heavy,Striker) \
  ENTRY(Medium,Weight) \
  ENTRY(Light,Heavy) \
  ENTRY(Medium,Heavy) \
  ENTRY(Armored,Heavy) \
  ENTRY(Super,Heavy) \
  ENTRY(Mega,Heavy) \

#define AS_ONE_VALUE_ENUM(name) name,
#define AS_TWO_VALUE_ENUM(a,b) a##b,
#define AS_ONE_VALUE_TEXT(name) Text(#name),
#define AS_TWO_VALUE_TEXT(a,b) Text(#a, #b),

Text TextTable [] = {
  Text(""),

  MEKTON_TYPES(AS_ONE_VALUE_TEXT)
  POWERPLANT_TYPE(AS_ONE_VALUE_TEXT)
  SERVO_LIST(AS_ONE_VALUE_TEXT)
  TEXT_ONE_VALUE_LIST(AS_ONE_VALUE_TEXT)
  TEXT_TWO_VALUE_LIST(AS_TWO_VALUE_TEXT)
  GRADE_LIST(AS_TWO_VALUE_TEXT)

  AS_ONE_VALUE_TEXT(<)
  AS_ONE_VALUE_TEXT(>)
};

enum class TextIndex {
  NONE,

  MEKTON_TYPES(AS_ONE_VALUE_ENUM)
  POWERPLANT_TYPE(AS_ONE_VALUE_ENUM)
  SERVO_LIST(AS_ONE_VALUE_ENUM)
  TEXT_ONE_VALUE_LIST(AS_ONE_VALUE_ENUM)
  TEXT_TWO_VALUE_LIST(AS_TWO_VALUE_ENUM)
  GRADE_LIST(AS_TWO_VALUE_ENUM)

  Less,
  Greater,

  COUNT
};