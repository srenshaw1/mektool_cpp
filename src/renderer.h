/*
File: 'renderer.cpp'
Author: Seth Renshaw
Date Created: 8/17/2016
*/

#pragma once

#include "SDL_helper.h"
#include "types.h"
#include "utils.h"
#include "string.hpp"

struct Renderer {
  SDL_Window   *window;
  SDL_Renderer *renderer;
  TTF_Font     *font;

  Renderer(char const *title, u32 width, u32 height) {
    window = SDL_CreateWindow(title,
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              width,
                              height,
                              SDL_WINDOW_BORDERLESS | SDL_WINDOW_SHOWN | SDL_WINDOW_INPUT_GRABBED);
    if (!window) {
      fprintf(stderr, "SDL_Error: %s\n", SDL_GetError());
      return;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
    if (!renderer) {
      fprintf(stderr, "SDL_Error: %s\n", SDL_GetError());
      return;
    }

    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    /* Init Font system */
    //NOTE(seth): Does the Font system need to be part of the renderer?
    if (TTF_Init() < 0) {
      fprintf(stderr, "TTF_Error: %s\n", TTF_GetError());
      return;
    }

    /* Load font */
    SDL_RWops *font_file = SDL_RWFromFile("zekton rg.ttf", "rb");
    if (!font_file) {
      fprintf(stderr, "SDL_Error: %s\n", SDL_GetError());
      return;
    }

    /* Open font */
    //NOTE(seth): This will free font_file once it's finished using it.
    font = TTF_OpenFontRW(font_file, 1, 12);
    if (!font) {
      fprintf(stderr, "SDL_Error: %s\n", SDL_GetError());
      return;
    }
  }

  void clearScreen() {
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_RenderClear(renderer);
  }

  void render() {
    SDL_RenderPresent(renderer);
  }

  ~Renderer() {
    if (font)
      TTF_CloseFont(font);
    if (TTF_WasInit())
      TTF_Quit();
    if (renderer)
      SDL_DestroyRenderer(renderer);
    if (window)
      SDL_DestroyWindow(window);
  }

  void renderLine(SDL_Color &color, u32 x0, u32 y0, u32 x1, u32 y1) {
    lineRGBA(renderer, x0, y0, x1, y1, color.r, color.g, color.b, color.a);
  }

  void renderPoly(SDL_Point array [], u32 size, b32 is_closed) {
    for (u32 i = 1; i < size; ++i)
      SDL_RenderDrawLine(renderer, array[i-1].x, array[i-1].y, array[i].x, array[i].y);
    if (is_closed)
      SDL_RenderDrawLine(renderer, array[size-1].x, array[size-1].y, array[0].x, array[0].y);
  }

  void renderString(String const &str, u32 x, u32 y) {
    SDL_Texture *str_texture = createTextTexture(str.text, getColor());
    SDL_Rect area = { x, y, 0, 0 };
    SDL_QueryTexture(str_texture, nullptr, nullptr, &area.w, &area.h);
    SDL_RenderCopy(renderer, str_texture, nullptr, &area);
    SDL_DestroyTexture(str_texture);
  }

  SDL_Texture *createTextTexture(char const *text, SDL_Color const &color) {
    SDL_Surface *text_surface = TTF_RenderUTF8_Blended(font, text, color);
    SDL_Texture *text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
    SDL_FreeSurface(text_surface);
    return text_texture;
  }

  inline
  SDL_Color getColor() {
    SDL_Color c = {};
    SDL_GetRenderDrawColor(renderer, &c.r, &c.g, &c.b, &c.a);
    return c;
  }

  inline
  void setColor(u8 r, u8 g, u8 b, u8 a) {
    SDL_SetRenderDrawColor(renderer, r, g, b, a);
  }

  inline
  void setColor(SDL_Color const &color) {
    setColor(color.r, color.g, color.b, color.a);
  }

  inline
  u8 getAlpha() {
    u8 alpha;
    SDL_GetRenderDrawColor(renderer, nullptr, nullptr, nullptr, &alpha);
    return alpha;
  }

  void setAlpha(SDL_Color const &color, r32 alpha_value) {
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_Color c = color;
    c.a = utils::lerp(0, color.a, alpha_value);
    setColor(c);
  }

  void renderTextureWithAlpha(SDL_Texture *texture, u32 x, u32 y, u32 w, u32 h) {
    SDL_SetTextureAlphaMod(texture, getAlpha());
    SDL_Color c = getColor();
    SDL_SetTextureColorMod(texture, c.r, c.g, c.b);
    SDL_Rect area = { x, y, w, h };
    SDL_RenderCopy(renderer, texture, nullptr, &area);
  }
};