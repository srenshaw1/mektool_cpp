#pragma once

#include "string.h"
#include "gui.h"

#include "darray.hpp"

#define MAX_ELEMENTS 12

TextIndex grade_list [] = {
  TextIndex::SuperLight,
  TextIndex::LightWeight,
  TextIndex::Striker,
  TextIndex::MediumStriker,
  TextIndex::HeavyStriker,
  TextIndex::MediumWeight,
  TextIndex::LightHeavy,
  TextIndex::MediumHeavy,
  TextIndex::ArmoredHeavy,
  TextIndex::SuperHeavy,
  TextIndex::MegaHeavy,
};

TextIndex servo_list [] = {
  TextIndex::Torso,
  TextIndex::Head,
  TextIndex::Arm,
  TextIndex::Leg,
  TextIndex::Wing,
  TextIndex::Tail,
  TextIndex::Pod,
};

TextIndex cockpit_list [] = {
  TextIndex::Closed,
  TextIndex::Open,
};

struct SelectorData {
  TextIndex value;
  u32 num_options;
  TextIndex* option_list;
  gui::AnimInfo* anim_list;
  s32 display_offset;

  SelectorData(TextIndex default_value, u32 list_length, TextIndex opt_list []) {
    anim_list = new gui::AnimInfo [list_length];

    value = default_value;
    num_options = list_length;
    option_list = opt_list;
    display_offset = 0;
  }

  ~SelectorData() {
    if (anim_list) {
      free(anim_list);
    }
  }

  void resetAnimList() {
    u32 start = Timer::RuntimeInSeconds();
    for (int i = 0; i < num_options; ++i) {
      anim_list[i].start_time = start + i * 0.1f;
    }
  }
};

struct EquipPanel {
  static u32 const num_components = 12;

  static u32 const w = 34;
  static u32 const h = 15;

  String text_cost;
  String text_spaces;
  String text_crew;
  b32 cbx_ejection_seat_enabled;
  b32 cbx_environment_seat_enabled;
  SelectorData* cockpit_type;
  gui::AnimInfo* anim_info_array;

  //NOTE(seth): This should not have a default value.
  EquipPanel(r32 start = 0)
  : text_cost("0"), text_spaces("0"), text_crew("0")
  , cbx_ejection_seat_enabled(false), cbx_environment_seat_enabled(false) {
    cockpit_type = new SelectorData(TextIndex::Type, ARRAY_COUNT(cockpit_list), cockpit_list);

    anim_info_array = new gui::AnimInfo [num_components];

    for (u32 t = 0; t < num_components; ++t) {
      anim_info_array[t].start_time = (start + t * 0.05f);
    }
  }

  ~EquipPanel() {
    if (cockpit_type) {
      delete cockpit_type;
    }
    if (anim_info_array) {
      //NOTE(seth): check whether I need this, once panels can be removed from the UI
      // for (u32 i = 0; i < num_components; ++i) {
      //   delete anim_info_array[i];
      // }
      delete [] anim_info_array;
    }
  }
};

struct EquipPanelData {
  static u32 const x = 80;
  static u32 const y =  5;
  static u32 const w = 40;
  static u32 const h = 75;

  static u32 const clip_x = 80;
  static u32 const clip_y =  8;
  static u32 const clip_w = 40;
  static u32 const clip_h = 71;

  DArray<EquipPanel>* panel_array = nullptr;
  s32 display_offset = 0;

  EquipPanelData()
  : display_offset(0) {
    panel_array = new DArray<EquipPanel>(12, 12);
  }

  ~EquipPanelData() {
    delete panel_array;
  }

  u32 getPanelSize() {
    return panel_array->size * EquipPanel::h;
  }
};

struct ServoPanel {
  static u32 const num_components = 31;

  static u32 const w = 34;
  static u32 const h = 24;

  String text_cost;
  String text_weight;
  String text_sp;
  String text_kills;
  String text_spaces;
  String text_wheel_val;
  String text_tread_val;
  String text_thrusters_val;
  String text_ges_val;

  b32 btn_wheel_dec_enabled;
  b32 btn_wheel_inc_enabled;
  b32 btn_tread_dec_enabled;
  b32 btn_tread_inc_enabled;
  b32 btn_thrusters_dec_enabled;
  b32 btn_thrusters_inc_enabled;
  b32 btn_ges_dec_enabled;
  b32 btn_ges_inc_enabled;

  SelectorData servo_type;
  SelectorData servo_grade;
  SelectorData armor_grade;

  DArray<EquipPanel> equip_info;
  gui::AnimInfo* anim_info_array;

  //NOTE(seth): this should not have a default value.
  ServoPanel(r32 start = 0)
  : text_cost("0"), text_weight("0"), text_sp("0"), text_kills("0"), text_spaces("0")
  , text_wheel_val("0"), text_tread_val("0"), text_thrusters_val("0"), text_ges_val("0")
  , btn_wheel_dec_enabled(true), btn_wheel_inc_enabled(true)
  , btn_tread_dec_enabled(true), btn_tread_inc_enabled(true)
  , btn_thrusters_dec_enabled(true), btn_thrusters_inc_enabled(true)
  , btn_ges_dec_enabled(true), btn_ges_inc_enabled(true)
  , servo_type(TextIndex::Type, ARRAY_COUNT(servo_list), servo_list)
  , servo_grade(TextIndex::Grade, ARRAY_COUNT(grade_list), grade_list)
  , armor_grade(TextIndex::Grade, ARRAY_COUNT(grade_list), grade_list)
  , equip_info(MAX_ELEMENTS, 12)
  {
    anim_info_array = new gui::AnimInfo [num_components];

    for (u32 t = 0; t < num_components; ++t) {
      anim_info_array[t].start_time = start + t * 0.05f;
    }
  }

  ~ServoPanel() {
    if (anim_info_array) {
      //NOTE(seth): check whether I need this, once panels can be removed from the UI
      // for (u32 i = 0; i < num_components; ++i) {
      //   delete anim_info_array[i];
      // }
      delete [] anim_info_array;
    }
  }
};

struct ServoPanelData {
  DArray<ServoPanel>* panel_array = nullptr;
  ServoPanel* selected_panel = nullptr;
  s32 display_offset = 0;

  static u32 const x = 40;
  static u32 const y =  5;
  static u32 const w = 40;
  static u32 const h = 75;

  static u32 const clip_x = 40;
  static u32 const clip_y =  8;
  static u32 const clip_w = 40;
  static u32 const clip_h = 71;

  ServoPanelData()
  : selected_panel(0), display_offset(0) {
    panel_array = new DArray<ServoPanel>(MAX_ELEMENTS, 12);
  }

  ~ServoPanelData() {
    if (panel_array) {
      delete panel_array;
    }
  }

  u32 getPanelSize() {
    return panel_array->size * ServoPanelData::h;
  }
};