/*
File: 'utils.h'
Author: Seth Renshaw
Date Created: 8/17/2016
*/

#pragma once

#include "types.h"

namespace utils {
  inline
  r32 lerp(r32 a, r32 b, r32 s) {
    if (s < 0) { return a; }
    if (s > 1) { return b; }
    return (a * (1-s)) + (b * (s));
  }
}