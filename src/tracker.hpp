/*
File: 'MekTool_CPP/tracker.h'
Author: Seth Renshaw
Date Created: 9/5/2016
*/

#ifndef TRACKER_H
#define TRACKER_H

#include "types.h"

struct Tracker {
  u32 x = 0;
  u32 y = 0;
  b32 is_tracking = false;

  inline
  b32 isInside(u32 other_x, u32 other_y, u32 w, u32 h) {
    return !(y < other_y || y > other_y + h ||
             x < other_x || x > other_x + w);
  }

  inline
  b32 isClicked(u32 other_x, u32 other_y, u32 w, u32 h) {
    return is_tracking && isInside(other_x, other_y, w, h);
  }
};

#endif