/*
File: 'app.cpp'
Author: Seth Renshaw
Date Created: 7/16/2016
*/

#pragma once

#include <stdio.h>
#include <assert.h>

#include "SDL_helper.h"
#include "string.h"
#include "text_table.h"
#include "timer.h"
#include "gui.h"
#include "tracker.h"
#include "commands.h"
#include "mekton.h"

#include "containers.h"

#define ARRAY_COUNT(array) (sizeof(array)/sizeof(*array))

global Color AppRed   = { 255,   0,   0, 255 };
global Color AppGreen = {   0, 255, 127, 255 };
global Color AppBlue  = {   0, 127, 255, 255 };

void handleButton(b32 clicked, b32 *enabled, List<cmd::Command *> *command_stack, cmd::Command *command) {
  if (!enabled) {
    return;
  }
  if (clicked) {
    if (*enabled) {
      command_stack->push_back(command);
    }
    free(command);
    *enabled = false;
  } else {
    *enabled = true;
  }
}

SelectorData *drawServoInfoPanel(Renderer *renderer, Text text_table [],
                                 Tracker *mouse, r32 accumulator,
                                 u32 offset_x, u32 offset_y,
                                 ServoPanelData *spd, List<cmd::Command *> *command_stack) {
  SelectorData *selector = nullptr;
  for (u32 panel_idx = 0; panel_idx < spd->panel_array->size; ++panel_idx) {
    u32 panel_x = offset_x + 2;
    u32 panel_y = offset_y + 5 + panel_idx  *(ServoPanel::h + 1);

    ServoPanel &servo_panel = spd->panel_array->get(panel_idx);

    gui::color = (&servo_panel == spd->selected_panel) ? AppGreen : AppBlue;

    u32 anim_idx = 0;
    gui::drawHorzPanel(renderer, text_table, accumulator, &servo_panel.anim_info_array[anim_idx++],
                       gui::PX(panel_x), gui::PX(panel_y),
                       gui::PX(ServoPanel::w), gui::PX(ServoPanel::h),
                       TextIndex::Servo);

    gui::Layout sub_layout = { panel_x + 1, panel_y + 2, panel_x + 1, panel_y + 2 };

    u32 h = 6;
    sub_layout.beginRow();
    if (gui::drawSelectorWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 10, h, TextIndex::Servo, servo_panel.servo_type.value) &&
        mouse->isInside(gui::PX(ServoPanelData::clip_x),
                       gui::PX(ServoPanelData::clip_y),
                       gui::PX(ServoPanelData::clip_w),
                       gui::PX(ServoPanelData::clip_h))) {
      spd->selected_panel = &spd->panel_array->get(panel_idx);
      selector = &servo_panel.servo_type;
    }
    if (gui::drawSelectorWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 11, h, TextIndex::Servo, servo_panel.servo_grade.value) &&
        mouse->isInside(gui::PX(ServoPanelData::clip_x),
                       gui::PX(ServoPanelData::clip_y),
                       gui::PX(ServoPanelData::clip_w),
                       gui::PX(ServoPanelData::clip_h))) {
      spd->selected_panel = &spd->panel_array->get(panel_idx);
      selector = &servo_panel.servo_grade;
    }
    if (gui::drawSelectorWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 11, h, TextIndex::Armor, servo_panel.armor_grade.value) &&
        mouse->isInside(gui::PX(ServoPanelData::clip_x),
                       gui::PX(ServoPanelData::clip_y),
                       gui::PX(ServoPanelData::clip_w),
                       gui::PX(ServoPanelData::clip_h))) {
      spd->selected_panel = &spd->panel_array->get(panel_idx);
      selector = &servo_panel.armor_grade;
    }
    sub_layout.endRow(h);

    h = 2;
    sub_layout.beginRow();
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 0,       TextIndex::Cost);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &servo_panel.text_cost, false, nullptr);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 1,       TextIndex::Weight);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &servo_panel.text_weight, false, nullptr);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 0,       TextIndex::SP);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &servo_panel.text_sp, false, nullptr);
    sub_layout.endRow(h);

    h = 2;
    sub_layout.beginRow();
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 0,       TextIndex::Kills);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &servo_panel.text_kills, false, nullptr);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 0,       TextIndex::Spaces);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &servo_panel.text_spaces, false, nullptr);
    sub_layout.endRow(h);

    sub_layout.beginRow();
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 0, TextIndex::MovementSystems);
    sub_layout.endRow(h);
    sub_layout.start_x += 1;

    sub_layout.beginRow();
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout,  2,       TextIndex::Wheel);

    handleButton(gui::drawButtonWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 3, h, TextIndex::Less) &&
                mouse->isInside(gui::PX(ServoPanelData::clip_x),
                               gui::PX(ServoPanelData::clip_y),
                               gui::PX(ServoPanelData::clip_w),
                               gui::PX(ServoPanelData::clip_h)) &&
                (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                &servo_panel.btn_wheel_dec_enabled,
                command_stack,
                cmd::createChangeSpinner(cmd::SpinnerChange::Decrement, &servo_panel.text_wheel_val));

    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 4, h, &servo_panel.text_wheel_val,        false,          nullptr);
    handleButton(gui::drawButtonWithLayout  (renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout,  2, 3, h, TextIndex::Greater) &&
                mouse->isInside(gui::PX(ServoPanelData::clip_x),
                               gui::PX(ServoPanelData::clip_y),
                               gui::PX(ServoPanelData::clip_w),
                               gui::PX(ServoPanelData::clip_h)) &&
                (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                &servo_panel.btn_wheel_inc_enabled,
                command_stack,
                cmd::createChangeSpinner(cmd::SpinnerChange::Increment, &servo_panel.text_wheel_val));

    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout,  0,       TextIndex::Tread);
    handleButton(gui::drawButtonWithLayout  (renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 3, h, TextIndex::Less) &&
                mouse->isInside(gui::PX(ServoPanelData::clip_x),
                               gui::PX(ServoPanelData::clip_y),
                               gui::PX(ServoPanelData::clip_w),
                               gui::PX(ServoPanelData::clip_h)) &&
                (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                &servo_panel.btn_tread_dec_enabled,
                command_stack,
                cmd::createChangeSpinner(cmd::SpinnerChange::Decrement, &servo_panel.text_tread_val));

    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 4, h, &servo_panel.text_tread_val, false, nullptr);
    handleButton(gui::drawButtonWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 2, 3, h, TextIndex::Greater) &&
                mouse->isInside(gui::PX(ServoPanelData::clip_x),
                               gui::PX(ServoPanelData::clip_y),
                               gui::PX(ServoPanelData::clip_w),
                               gui::PX(ServoPanelData::clip_h)) &&
                (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                &servo_panel.btn_tread_inc_enabled,
                command_stack,
                cmd::createChangeSpinner(cmd::SpinnerChange::Increment, &servo_panel.text_tread_val));
    sub_layout.endRow(h);

    sub_layout.beginRow();
    gui::drawLabelWithLayout(renderer, text_table, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, 0, TextIndex::Thrusters);
    handleButton(gui::drawButtonWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 3, h, TextIndex::Less) &&
                 mouse->isInside(gui::PX(ServoPanelData::clip_x),
                                gui::PX(ServoPanelData::clip_y),
                                gui::PX(ServoPanelData::clip_w),
                                gui::PX(ServoPanelData::clip_h)) &&
                 (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                 &servo_panel.btn_thrusters_dec_enabled,
                 command_stack,
                 cmd::createChangeSpinner(cmd::SpinnerChange::Decrement, &servo_panel.text_thrusters_val));

    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 4, h, &servo_panel.text_thrusters_val,        false,          nullptr);
    handleButton(gui::drawButtonWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout,  2, 3, h, TextIndex::Greater) &&
                 mouse->isInside(gui::PX(ServoPanelData::clip_x),
                                gui::PX(ServoPanelData::clip_y),
                                gui::PX(ServoPanelData::clip_w),
                                gui::PX(ServoPanelData::clip_h)) &&
                 (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                 &servo_panel.btn_thrusters_inc_enabled,
                 command_stack,
                 cmd::createChangeSpinner(cmd::SpinnerChange::Increment, &servo_panel.text_thrusters_val));

    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout,  1,       TextIndex::GES);
    handleButton(gui::drawButtonWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 3, h, TextIndex::Less) &&
                 mouse->isInside(gui::PX(ServoPanelData::clip_x),
                                gui::PX(ServoPanelData::clip_y),
                                gui::PX(ServoPanelData::clip_w),
                                gui::PX(ServoPanelData::clip_h)) &&
                 (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                 &servo_panel.btn_ges_dec_enabled,
                 command_stack,
                 cmd::createChangeSpinner(cmd::SpinnerChange::Decrement, &servo_panel.text_ges_val));

    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout, -1, 4, h, &servo_panel.text_ges_val,        false,          nullptr);
    handleButton(gui::drawButtonWithLayout(renderer, text_table, mouse, accumulator, &servo_panel.anim_info_array[anim_idx++], &sub_layout,  2, 3, h, TextIndex::Greater) &&
                 mouse->isInside(gui::PX(ServoPanelData::clip_x),
                                gui::PX(ServoPanelData::clip_y),
                                gui::PX(ServoPanelData::clip_w),
                                gui::PX(ServoPanelData::clip_h)) &&
                 (spd->selected_panel = &spd->panel_array->get(panel_idx)),
                 &servo_panel.btn_ges_inc_enabled,
                 command_stack,
                 cmd::createChangeSpinner(cmd::SpinnerChange::Increment, &servo_panel.text_ges_val));
    sub_layout.endRow(h);
  }
  return selector;

error:
  return nullptr;
}

SelectorData *drawEquipInfoPanel(Renderer *renderer, Text text_table [], Tracker *mouse,
                                 r32 accumulator,
                                 u32 offset_x, u32 offset_y, EquipPanelData *epd) {
  SelectorData *selector = nullptr;
  for (u32 panel_idx = 0; panel_idx < epd->panel_array->size; ++panel_idx) {
    u32 panel_x = offset_x + 2;
    u32 panel_y = offset_y + 5 + panel_idx  *(EquipPanel::h + 1);

    EquipPanel &equip_panel = epd->panel_array->get(panel_idx);
    u32 anim_idx = 0;
    gui::drawHorzPanel(renderer, text_table, accumulator, &equip_panel.anim_info_array[anim_idx++],
                       gui::PX(panel_x), gui::PX(panel_y),
                       gui::PX(EquipPanel::w), gui::PX(EquipPanel::h),
                       TextIndex::Equipment);

    gui::Layout sub_layout  = {
      panel_x + 1, panel_y + 2,
      panel_x + 1, panel_y + 2
    };

    u32 h = 6;
    sub_layout.beginRow();
    if (gui::drawSelectorWithLayout(renderer, text_table, mouse, accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 18, h, TextIndex::Cockpit, equip_panel.cockpit_type->value) &&
        mouse->isInside(gui::PX(EquipPanelData::clip_x),
                       gui::PX(EquipPanelData::clip_y),
                       gui::PX(EquipPanelData::clip_w),
                       gui::PX(EquipPanelData::clip_h))) {
      selector = equip_panel.cockpit_type;
    }
    sub_layout.endRow(h);

    h = 2;
    sub_layout.beginRow();
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1,       TextIndex::Cost);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &equip_panel.text_cost, false, nullptr);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1,       TextIndex::Spaces);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &equip_panel.text_spaces, false, nullptr);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1,       TextIndex::Crew);
    gui::drawTextEditWithLayout(renderer, &text_table[(u32)TextIndex::NONE], mouse, accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, 4, h, &equip_panel.text_crew, false, nullptr);
    sub_layout.endRow(h);

    h = 2;
    sub_layout.beginRow();
    gui::drawCheckboxWithLayout(renderer,             mouse, accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, &equip_panel.cbx_ejection_seat_enabled);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, TextIndex::EjectionSeat);
    gui::drawCheckboxWithLayout(renderer,             mouse, accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, &equip_panel.cbx_environment_seat_enabled);
    gui::drawLabelWithLayout   (renderer, text_table,        accumulator, &equip_panel.anim_info_array[anim_idx++], &sub_layout, 1, TextIndex::EnvironmentSeat);
    sub_layout.endRow(h);
  }
  return selector;
}

TextIndex form_list [] = {
  TextIndex::Beast,
  TextIndex::Fighter,
  TextIndex::Hybrid,
  TextIndex::Aquamecha,
};

TextIndex powerplant_list [] = {
  TextIndex::Cool,
  TextIndex::Hot,
};

struct App {
  b32 running = true;

  Database db;

  Tracker mouse = {};
  Timer   timer = {};

  List<cmd::Command *> *command_stack;
  u32 previous_command_stack_count;

  ServoPanelData *servo_panel_data;
  EquipPanelData *equip_panel_data;

  String text_mekton_name;

  Renderer renderer;

  gui::AnimInfo anim_btn_clear;
  gui::AnimInfo anim_btn_export;
  gui::AnimInfo anim_btn_save;
  gui::AnimInfo anim_btn_load;
  gui::AnimInfo anim_btn_exit;
  gui::AnimInfo anim_mecha_info;
  gui::AnimInfo anim_weapon_links;
  gui::AnimInfo anim_servo_info;
  gui::AnimInfo anim_equip_info;
  gui::AnimInfo anim_mi_cost;
  gui::AnimInfo anim_mi_cost_val;
  gui::AnimInfo anim_mi_crew;
  gui::AnimInfo anim_mi_crew_val;
  gui::AnimInfo anim_mi_form;
  gui::AnimInfo anim_mi_gesma;
  gui::AnimInfo anim_mi_gesma_dec;
  gui::AnimInfo anim_mi_gesma_inc;
  gui::AnimInfo anim_mi_gesma_val;
  gui::AnimInfo anim_mi_moveall;
  gui::AnimInfo anim_mi_moveall_val;
  gui::AnimInfo anim_mi_moveval;
  gui::AnimInfo anim_mi_moveval_val;
  gui::AnimInfo anim_mi_name;
  gui::AnimInfo anim_mi_powerplant;
  gui::AnimInfo anim_mi_thrusterma;
  gui::AnimInfo anim_mi_thrusterma_dec;
  gui::AnimInfo anim_mi_thrusterma_inc;
  gui::AnimInfo anim_mi_thrusterma_val;
  gui::AnimInfo anim_mi_treads;
  gui::AnimInfo anim_mi_weight;
  gui::AnimInfo anim_mi_weight_val;
  gui::AnimInfo anim_mi_wheels;
  gui::AnimInfo anim_si_add;
  gui::AnimInfo anim_si_rem;
  gui::AnimInfo anim_ei_add;
  gui::AnimInfo anim_ei_rem;

  String text_mi_weight_val;
  String text_mi_cost_val;
  String text_mi_moveval_val;
  String text_mi_thrusterma_val;
  String text_mi_crew_val;
  String text_mi_moveall_val;
  String text_mi_gesma_val;

  b32 btn_clear_enabled = true;
  b32 btn_export_enabled = true;
  b32 btn_save_enabled = true;
  b32 btn_load_enabled = true;
  b32 btn_exit_enabled = true;
  b32 btn_mi_thrusterma_dec_enabled = true;
  b32 btn_mi_thrusterma_inc_enabled = true;
  b32 btn_mi_gesma_dec_enabled = true;
  b32 btn_mi_gesma_inc_enabled = true;
  b32 btn_si_add_enabled = true;
  b32 btn_ei_add_enabled = true;

  SelectorData *select_form       = new SelectorData(TextIndex::Type, ARRAY_COUNT(form_list), form_list);
  SelectorData *select_powerplant = new SelectorData(TextIndex::Type, ARRAY_COUNT(powerplant_list), powerplant_list);
  SelectorData *select_wheels     = new SelectorData(TextIndex::Grade, ARRAY_COUNT(grade_list), grade_list);
  SelectorData *select_treads     = new SelectorData(TextIndex::Grade, ARRAY_COUNT(grade_list), grade_list);

  SelectorData *enabled_selector = nullptr;
  u32 option_list_offset = 0;

  App(u32 width, u32 height)
  : renderer("MekTool", width, height)
  , text_mekton_name("") {
    command_stack = new List<cmd::Command *>();
    previous_command_stack_count = command_stack->count;

    servo_panel_data = new ServoPanelData();
    check_mem(servo_panel_data);

    equip_panel_data = new EquipPanelData();
    check_mem(equip_panel_data);

    db.open("mektool.db", "US");

    // TextTable[(u32)TextIndex::NONE].loadTexture(&renderer);
    // TextTable[(u32)TextIndex::Greater].loadTexture(&renderer);
    // TextTable[(u32)TextIndex::Less].loadTexture(&renderer);
    // for (u32 i = 1; i < (u32)TextIndex::COUNT - 2; ++i)
    //   TextTable[i].loadTextureFromDB(&db, &renderer);

    /* Animation Code */
    float const small_inc = 0.05f;
    float const large_inc = 0.15f;
    float t = 0.00f;
    anim_btn_clear  = gui::AnimInfo(t); t += large_inc;
    anim_btn_export = gui::AnimInfo(t); t += large_inc;
    anim_btn_save   = gui::AnimInfo(t); t += large_inc;
    anim_btn_load   = gui::AnimInfo(t); t += large_inc;
    anim_btn_exit   = gui::AnimInfo(t); t += large_inc;

    t = 0.00f;
    anim_mecha_info   = gui::AnimInfo(t); t += large_inc;
    anim_weapon_links = gui::AnimInfo(t); t += large_inc;
    anim_servo_info   = gui::AnimInfo(t); t += large_inc;
    anim_equip_info   = gui::AnimInfo(t); t += large_inc;

    t = anim_mecha_info.fade_time + anim_mecha_info.extend_time + small_inc;
    anim_mi_cost           = gui::AnimInfo(t); t += small_inc;
    anim_mi_cost_val       = gui::AnimInfo(t); t += small_inc;
    anim_mi_crew           = gui::AnimInfo(t); t += small_inc;
    anim_mi_crew_val       = gui::AnimInfo(t); t += small_inc;
    anim_mi_form           = gui::AnimInfo(t); t += small_inc;
    anim_mi_gesma          = gui::AnimInfo(t); t += small_inc;
    anim_mi_gesma_dec      = gui::AnimInfo(t); t += small_inc;
    anim_mi_gesma_inc      = gui::AnimInfo(t); t += small_inc;
    anim_mi_gesma_val      = gui::AnimInfo(t); t += small_inc;
    anim_mi_moveall        = gui::AnimInfo(t); t += small_inc;
    anim_mi_moveall_val    = gui::AnimInfo(t); t += small_inc;
    anim_mi_moveval        = gui::AnimInfo(t); t += small_inc;
    anim_mi_moveval_val    = gui::AnimInfo(t); t += small_inc;
    anim_mi_name           = gui::AnimInfo(t); t += small_inc;
    anim_mi_powerplant     = gui::AnimInfo(t); t += small_inc;
    anim_mi_thrusterma     = gui::AnimInfo(t); t += small_inc;
    anim_mi_thrusterma_dec = gui::AnimInfo(t); t += small_inc;
    anim_mi_thrusterma_inc = gui::AnimInfo(t); t += small_inc;
    anim_mi_thrusterma_val = gui::AnimInfo(t); t += small_inc;
    anim_mi_treads         = gui::AnimInfo(t); t += small_inc;
    anim_mi_weight         = gui::AnimInfo(t); t += small_inc;
    anim_mi_weight_val     = gui::AnimInfo(t); t += small_inc;
    anim_mi_wheels         = gui::AnimInfo(t); t += small_inc;

    t = anim_servo_info.fade_time + anim_servo_info.extend_time + small_inc;
    anim_si_add = gui::AnimInfo(t); t += small_inc;
    anim_si_rem = gui::AnimInfo(t); t += small_inc;

    t = anim_equip_info.fade_time + anim_equip_info.extend_time + small_inc;
    anim_ei_add = gui::AnimInfo(t); t += small_inc;
    anim_ei_rem = gui::AnimInfo(t); t += small_inc;

    text_mi_weight_val     = String("0");
    text_mi_cost_val       = String("0");
    text_mi_moveval_val    = String("0");
    text_mi_thrusterma_val = String("0");
    text_mi_crew_val       = String("0");
    text_mi_moveall_val    = String("0");
    text_mi_gesma_val      = String("0");

    return;
  error:
    debug("Unable to instantiate App");
    return;
  }

  void setEnabledSelector(SelectorData *selector) {
    enabled_selector = selector;
    enabled_selector->resetAnimList();
    option_list_offset = 0;
  }

  void setMouseTracking(b32 enabled) {
    mouse.is_tracking = enabled;
    gui::update_ui = true;
  }

  void setMousePos(u32 x, u32 y) {
    mouse.x = x;
    mouse.y = y;
  }

  void addScrollValue(s32 scroll_value) {
    u32 const list_panel_mod = 2;
    u32 const message_panel_mod = gui::PX(2);
    if (mouse.isInside(gui::PX(ServoPanelData::x),
                       gui::PX(ServoPanelData::y),
                       gui::PX(ServoPanelData::x + ServoPanelData::w),
                       gui::PX(ServoPanelData::y + ServoPanelData::h))) {
      s32 offset = servo_panel_data->display_offset + scroll_value  *list_panel_mod;

      // min_scroll = max_scroll_height - servo_panel_size
      s32 min_scroll = (servo_panel_data->panel_array->size > 2)
        ? (-6 + -25  *(servo_panel_data->panel_array->size - 3))
        : 0;

      if (offset < min_scroll) {
        offset = min_scroll;
      } else if (offset > 0) {
        offset = 0;
      }
      servo_panel_data->display_offset = offset;

    } else if (mouse.isInside(gui::PX(EquipPanelData::x),
                              gui::PX(EquipPanelData::y),
                              gui::PX(EquipPanelData::x + EquipPanelData::w),
                              gui::PX(EquipPanelData::y + EquipPanelData::h))) {
      s32 offset = equip_panel_data->display_offset + scroll_value  *list_panel_mod;

      //NOTE(seth): fix the scroll values
      s32 min_scroll = (equip_panel_data->panel_array->size > 5)
        ? (-6 + -16  *(equip_panel_data->panel_array->size - 6))
        : 0;

      if (offset < min_scroll) {
        offset = min_scroll;
      } else if (offset > 0) {
        offset = 0;
      }
      equip_panel_data->display_offset = offset;

    } else if (mouse.isInside(0,0,720,24)) {
      if (enabled_selector) {
        s32 offset = enabled_selector->display_offset + scroll_value  *message_panel_mod;

        s32 min_scroll = 0;
        for (u32 i = 0; i < enabled_selector->num_options; ++i) {
          u32 text_width = TextTable[(u32)enabled_selector->option_list[i]].textSize(false).x / 8;
          min_scroll -= text_width + gui::PX(2);
        }
        printf("Offset: %d, Min: %d\n", offset, min_scroll);

        if (offset < min_scroll) {
          offset = min_scroll;
        } else if (offset > 0) {
          offset = 0;
        }
        enabled_selector->display_offset = offset;
      }
    }
    gui::update_ui = true;
  }

  void setInputText(char const *text) {
    text_mekton_name.append(text);
    gui::update_ui = true;
  }

  void removeText(u32 num_chars) {
    for (u32 i = 0; i < num_chars; ++i) {
      text_mekton_name.pop();
    }
    gui::update_ui = true;
  }

  void shutdown() {
    running = false;
  }

  void update() {
    timer.update();

    if (gui::update_ui) {
      gui::update_ui = false;

      renderer.clearScreen();

      /* Render Code */
      gui::color = AppBlue;

      if (enabled_selector) {
        u32 x = 0;
        gui::setClipRect(&renderer, 0,0,90,4);
        for (int i = 0; i < enabled_selector->num_options; ++i) {
          u32 text_width = TextTable[(u32)enabled_selector->option_list[i]].textSize(false).x;
          if (i > 0) {
            Color color = renderer.getColor();
            renderer.renderLine(color,
              gui::PX(x) + enabled_selector->display_offset, gui::PX(0),
              gui::PX(x) + enabled_selector->display_offset, gui::PX(3));
          }
          x += 1;
          if (gui::drawLabel(&renderer, TextTable, timer.accumulator, &enabled_selector->anim_list[i],
                             gui::PX(x) + enabled_selector->display_offset,
                             gui::PX(1), enabled_selector->option_list[i], &mouse)) {
            enabled_selector->value = enabled_selector->option_list[i];
            enabled_selector = nullptr;
            break;
          }
          x += (text_width / TILE_SIZE + 1) + 1;
        }
        gui::clearClipRect(&renderer);
      }

      gui::Layout layout = {};
      u32 h = 2;

      layout.x = 90;
      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_btn_clear, &layout, 0, 6, h, TextIndex::Clear),
                   &btn_clear_enabled,
                   command_stack,
                   cmd::createClearData());

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_btn_export, &layout, 0, 7, h, TextIndex::Export),
                   &btn_export_enabled,
                   command_stack,
                   cmd::createExportData());

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_btn_save, &layout, 0, 6, h, TextIndex::Save),
                   &btn_save_enabled,
                   command_stack,
                   cmd::createSaveData(&db));

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_btn_load, &layout, 0, 6, h, TextIndex::Load),
                   &btn_load_enabled,
                   command_stack,
                   cmd::createLoadData(&db));

      if (gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_btn_exit, &layout, 0, 5, h, TextIndex::Exit)) {
        if (btn_exit_enabled) {
          running = false;
        }
        btn_exit_enabled = false;
      } else {
        btn_exit_enabled = true;
      }
      layout.endRow(h, 2);

      renderer.renderLine(gui::color, gui::PX(0), gui::PX(3), gui::PX(120), gui::PX(3));

      layout.x = 1;
      layout.start_y = 4;
      gui::drawVertPanel(&renderer, TextTable, timer.accumulator, &anim_mecha_info, gui::PX(layout.x), gui::PX(layout.y), gui::PX(38), gui::PX(27), TextIndex::MechaInfo);
      layout.start_x = 2;
      layout.y += 4;

      // Mecha Column
      layout.beginRow();
      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::Name], &mouse, timer.accumulator, &anim_mi_name,       &layout, 2, 14, h, &text_mekton_name, true, nullptr);
      gui::drawLabelWithLayout   (&renderer, TextTable,        timer.accumulator, &anim_mi_weight,     &layout, 0,        TextIndex::Weight);
      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_weight_val, &layout, 2,  4, h, &text_mi_weight_val, false, nullptr);
      gui::drawLabelWithLayout   (&renderer, TextTable,        timer.accumulator, &anim_mi_cost,       &layout, 1,        TextIndex::Cost);
      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_cost_val,   &layout, 2,  4, h, &text_mi_cost_val, false, nullptr);
      layout.endRow(h);

      layout.beginRow();
      gui::drawLabelWithLayout   (&renderer, TextTable,        timer.accumulator, &anim_mi_moveval,        &layout,  0,       TextIndex::MV);
      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_moveval_val,    &layout,  1, 4, h, &text_mi_moveval_val, false, nullptr);
      gui::drawLabelWithLayout   (&renderer, TextTable,        timer.accumulator, &anim_mi_thrusterma,     &layout,  1,       TextIndex::ThrusterMA);

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_thrusterma_dec, &layout, -1, 3, h, TextIndex::Less),
                   &btn_mi_thrusterma_dec_enabled,
                   command_stack,
                   cmd::createChangeSpinner(cmd::SpinnerChange::Decrement, &text_mi_thrusterma_val));

      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_thrusterma_val, &layout, -1, 4, h, &text_mi_thrusterma_val, false, nullptr);

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_thrusterma_inc, &layout,  2, 3, h, TextIndex::Greater),
                   &btn_mi_thrusterma_inc_enabled,
                   command_stack,
                   cmd::createChangeSpinner(cmd::SpinnerChange::Increment, &text_mi_thrusterma_val));

      gui::drawLabelWithLayout   (&renderer, TextTable,        timer.accumulator, &anim_mi_crew,           &layout,  0,       TextIndex::Crew);
      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_crew_val,       &layout,  2, 4, h, &text_mi_crew_val, false, nullptr);
      layout.endRow(h);

      layout.beginRow();
      gui::drawLabelWithLayout(&renderer, TextTable, timer.accumulator, &anim_mi_moveall, &layout, 0, TextIndex::MA);
      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_moveall_val, &layout, 1, 4, h, &text_mi_moveall_val, false, nullptr);
      gui::drawLabelWithLayout(&renderer, TextTable, timer.accumulator, &anim_mi_gesma, &layout, 4, TextIndex::GESMA);

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_gesma_dec,   &layout, -1, 3, h, TextIndex::Less),
                   &btn_mi_gesma_dec_enabled,
                   command_stack,
                   cmd::createChangeSpinner(cmd::SpinnerChange::Decrement, &text_mi_gesma_val));

      gui::drawTextEditWithLayout(&renderer, &TextTable[(u32)TextIndex::NONE], &mouse, timer.accumulator, &anim_mi_gesma_val,   &layout, -1, 4, h, &text_mi_gesma_val, false, nullptr);

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_gesma_inc,   &layout,  2, 3, h, TextIndex::Greater),
                   &btn_mi_gesma_inc_enabled,
                   command_stack,
                   cmd::createChangeSpinner(cmd::SpinnerChange::Increment, &text_mi_gesma_val));
      layout.endRow(h);

      h = 6;
      layout.beginRow();
      if (gui::drawSelectorWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_form, &layout, 18, h, TextIndex::Form, select_form->value)) {
        setEnabledSelector(select_form);
      }
      if (gui::drawSelectorWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_powerplant, &layout, 18, h, TextIndex::Powerplant, select_powerplant->value)) {
        setEnabledSelector(select_powerplant);
      }
      layout.endRow(h);

      layout.beginRow();
      if (gui::drawSelectorWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_wheels, &layout, 18, h, TextIndex::Wheels, select_wheels->value)) {
        setEnabledSelector(select_wheels);
      }
      if (gui::drawSelectorWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_mi_treads, &layout, 18, h, TextIndex::Treads, select_treads->value)) {
        setEnabledSelector(select_treads);
      }
      layout.endRow(h);

      layout.y += 1;

      layout.x =  1;
      gui::drawVertPanel(&renderer, TextTable, timer.accumulator, &anim_weapon_links, gui::PX(layout.x), gui::PX(layout.y), gui::PX(38), gui::PX(79 - layout.y), TextIndex::WeaponLinks);

      // Servo Column
      h = 2;

      layout.x = 41;
      layout.y = layout.start_y;
      gui::drawVertPanel(&renderer, TextTable, timer.accumulator, &anim_servo_info, gui::PX(layout.x), gui::PX(layout.y), gui::PX(38), gui::PX(79 - layout.y), TextIndex::ServoList, 4);
      layout.start_x = layout.x;

      layout.x += 27;
      layout.y += 1;

      handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_si_add, &layout, 0, 10, h, TextIndex::AddServo),
                   &btn_si_add_enabled,
                   command_stack,
                   cmd::createAddServo(&db));

      gui::setClipRect(&renderer,
                       ServoPanelData::clip_x,
                       ServoPanelData::clip_y,
                       ServoPanelData::clip_w,
                       ServoPanelData::clip_h);
      SelectorData *servo_selector = drawServoInfoPanel(&renderer, TextTable, &mouse, timer.accumulator, layout.start_x, layout.y + servo_panel_data->display_offset, servo_panel_data, command_stack);
      if (servo_selector) {
        enabled_selector = servo_selector;
      }
      gui::clearClipRect(&renderer);

      // Equipment Column
      gui::color = AppBlue;
      layout.x = 81;
      layout.y = layout.start_y;
      gui::drawVertPanel(&renderer, TextTable, timer.accumulator, &anim_equip_info, gui::PX(layout.x), gui::PX(layout.y), gui::PX(38), gui::PX(79 - layout.y), TextIndex::EquipmentList, 4);
      layout.start_x = layout.x;

      layout.x += 25;
      layout.y += 1;
      if (servo_panel_data->selected_panel) {
        handleButton(gui::drawButtonWithLayout(&renderer, TextTable, &mouse, timer.accumulator, &anim_ei_add, &layout, 0, 12, h, TextIndex::AddEquipment),
                     &btn_ei_add_enabled,
                     command_stack,
                     cmd::createAddEquipment(servo_panel_data->selected_panel));
        equip_panel_data->panel_array = &servo_panel_data->selected_panel->equip_info;
      } else {
        equip_panel_data->panel_array = nullptr;
      }

      if (equip_panel_data->panel_array &&
          equip_panel_data->panel_array->size > 0) {
        gui::setClipRect(&renderer,
                         EquipPanelData::clip_x,
                         EquipPanelData::clip_y,
                         EquipPanelData::clip_w,
                         EquipPanelData::clip_h);
        SelectorData *equip_selector = drawEquipInfoPanel(&renderer, TextTable, &mouse, timer.accumulator, layout.start_x, layout.y + equip_panel_data->display_offset, equip_panel_data);
        if (equip_selector) {
          enabled_selector = equip_selector;
        }
        gui::clearClipRect(&renderer);
      }

      renderer.render();
    } else {
      // printf("no update\n");
    }

    if (command_stack->count > previous_command_stack_count) {
      command_stack->last->data->apply();
      previous_command_stack_count = command_stack->count;
    }

#if DEBUG
    char time [128];
    sprintf(time, "Time: %f", timer.accumulator);
    SDL_Color white = { 255, 255, 255, 255 };
    SDL_Color black = { 0, 0, 0, 255 };
    SDL_Texture *time_texture = renderer.createTextTexture(time, white);
    s32 time_w, time_h;
    SDL_QueryTexture(time_texture, nullptr, nullptr, &time_w, &time_h);
    SDL_Rect time_rect = { 0, 0, time_w, time_h };
    renderer.setColor(black);
    SDL_RenderFillRect(renderer.renderer, &time_rect);
    renderer.setColor(white);
    renderer.renderTextureWithAlpha(time_texture, 0, 0, time_w, time_h);
    SDL_DestroyTexture(time_texture);
    renderer.render();
#endif

    return;
  error:
    debug("Exiting update() with error");
  }
};