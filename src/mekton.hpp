/*
Author: Seth Renshaw
Date Created: 11/1/2016
*/

#pragma once

#include "string.hpp"
#include "list.hpp"

#define FORM_NAMES(ENTRY)\
  ENTRY(Mekton)\
  ENTRY(Mechabeast)\
  ENTRY(Mechatank)\
  ENTRY(Mechafighter)\

#define POWERPLANT_NAMES(ENTRY)\
  ENTRY(Cool)\
  ENTRY(Hot)\

#define SERVO_NAMES(ENTRY)\
  ENTRY(Torso)\
  ENTRY(Arm)\
  ENTRY(Leg)\
  ENTRY(Head)\
  ENTRY(Tail)\
  ENTRY(Wing)\
  ENTRY(Pod)\

#define GRADE_NAMES(ENTRY)\
  ENTRY(SuperLight,    "Super Light")\
  ENTRY(LightWeight,   "Light Weight")\
  ENTRY(Striker,       "Striker")\
  ENTRY(MediumStriker, "Medium Striker")\
  ENTRY(HeavyStriker,  "Heavy Striker")\
  ENTRY(MediumWeight,  "Medium Weight")\
  ENTRY(LightHeavy,    "Light Heavy")\
  ENTRY(MediumHeavy,   "Medium Heavy")\
  ENTRY(ArmoredHeavy,  "Armored Heavy")\
  ENTRY(SuperHeavy,    "Super Heavy")\
  ENTRY(MegaHeavy,     "Mega Heavy")\

#define EQUIPMENT_TYPES(ENTRY)\
  ENTRY(Beam,        "Beam")\
  ENTRY(Projectile,  "Projectile")\
  ENTRY(Missile,     "Missile")\
  ENTRY(Melee,       "Melee")\
  ENTRY(EnergyMelee, "Energy Melee")\
  ENTRY(ArmExtremity, "Arm Extremity")\
  ENTRY(LegExtremity, "Leg Extremity")\
  ENTRY(Cockpit,      "Cockpit")\
  ENTRY(Sensor,       "Sensor")\
  ENTRY(Shield,       "Shield")\
  ENTRY(Assembly,     "Assembly")\

#define WEAPON_NAMES(ENTRY)\
  ENTRY(LightBeamGun,    "Light Beam Gun")\
  ENTRY(MediumBeamGun,   "Medium Beam Gun")\
  ENTRY(HeavyBeamGun,    "Heavy Beam Gun")\
  ENTRY(BeamCannon,      "Beam Cannon")\
  ENTRY(HeavyBeamCannon, "Heavy Beam Cannon")\
  ENTRY(NovaCannon,      "Nova Cannon")\
  ENTRY(PulseCannon,     "Pulse Cannon")\
  ENTRY(BeamSweeper,     "Beam Sweeper")\
  ENTRY(LightCannon,     "Light Cannon")\
  ENTRY(MediumCannon,    "Medium Cannon")\
  ENTRY(HeavyCannon,     "Heavy Cannon")\
  ENTRY(GiantCannon,     "Giant Cannon")\
  ENTRY(Autocannon,      "Autocannon ")\
  ENTRY(HeavyAutocannon, "Heavy Autocannon")\
  ENTRY(EpoxyGun,        "Epoxy Gun")\
  ENTRY(RocketPod,       "Rocket Pod")\
  ENTRY(RocketLauncher,  "Rocket Launcher")\
  ENTRY(MissilePod,      "Missile Pod")\
  ENTRY(HeavyMissile,    "Heavy Missile")\
  ENTRY(Sword,           "Sword")\
  ENTRY(Axe,             "Axe")\
  ENTRY(Mace,            "Mace")\
  ENTRY(Drill,           "Drill")\
  ENTRY(Saw,             "Saw")\
  ENTRY(ShockWhip,       "Shock-Whip")\
  ENTRY(EnergySword,     "Energy Sword")\
  ENTRY(EnergyAxe,       "Energy Axe")\
  ENTRY(EnergyLance,     "Energy Lance")\
  ENTRY(NovaSword,       "Nova Sword")\

#define ARM_EXT_NAMES(ENTRY)\
  ENTRY(Hand,     "Hand")\
  ENTRY(ArmClaw,  "Claw")\
  ENTRY(ArmTalon, "Talon")\
  ENTRY(Pincer,   "Pincer")\

#define LEG_EXT_NAMES(ENTRY)\
  ENTRY(Foot,     "Foot")\
  ENTRY(LegClaw,  "Claw")\
  ENTRY(LegTalon, "Talon")\

#define COCKPIT_NAMES(ENTRY)\
  ENTRY(MainCockpit,    "Main Cockpit")\
  ENTRY(PassengerSpace, "Passenger Space")\
  ENTRY(EjectionSeat,   "Ejection Seat")\
  ENTRY(EnvironmentPod, "Environment Pod")\

#define SENSOR_NAMES(ENTRY)\
  ENTRY(MainSensor,   "Main Sensor")\
  ENTRY(BackupSensor, "Backup Sensor")\

#define SHIELD_NAMES(ENTRY)\
  ENTRY(Small,  "Small")\
  ENTRY(Medium, "Medium")\
  ENTRY(Large,  "Large")\

#define ASSEMBLY_NAMES(ENTRY)\
  ENTRY(AntiTheftCodeLock,    "Anti-Theft Code Lock")\
  ENTRY(AntiTheftCodeAlarm,   "Anti-Theft Code Alarm")\
  ENTRY(DamageControlPackage, "Damage Control Package")\
  ENTRY(Spotlights,           "Spotlights")\
  ENTRY(Liftwire,             "Liftwire")\
  ENTRY(Micromanipulators,    "Micromanipulators")\
  ENTRY(StereoSystem,         "Stereo System")\
  ENTRY(StorageModule,        "Storage Module")\
  ENTRY(WeaponLink,           "Weapon Link")\

#define ENUM(item) item,
enum class Form {
  NONE,
  FORM_NAMES(ENUM)
};

enum class Powerplant {
  NONE,
  POWERPLANT_NAMES(ENUM)
};

enum class ServoType {
  NONE,
  SERVO_NAMES(ENUM)
};
#undef ENUM

#define ENUM(item, string) item,
enum class Grade {
  NONE,
  GRADE_NAMES(ENUM)
};

enum class EquipmentType {
  EQUIPMENT_TYPES(ENUM)
};

enum class Equipment {
  NONE,
  WEAPON_NAMES(ENUM)
  ARM_EXT_NAMES(ENUM)
  LEG_EXT_NAMES(ENUM)
  COCKPIT_NAMES(ENUM)
  SENSOR_NAMES(ENUM)
  SHIELD_NAMES(ENUM)
  ASSEMBLY_NAMES(ENUM)
};
#undef ENUM

#define STRING(item) String const tx##item(#item);
FORM_NAMES(STRING)

POWERPLANT_NAMES(STRING)

SERVO_NAMES(STRING)

#undef STRING

#define STRING(item,string) String const tx##item(string);
GRADE_NAMES(STRING)

EQUIPMENT_TYPES(STRING)

WEAPON_NAMES(STRING)
ARM_EXT_NAMES(STRING)
LEG_EXT_NAMES(STRING)
COCKPIT_NAMES(STRING)
SENSOR_NAMES(STRING)
SHIELD_NAMES(STRING)
ASSEMBLY_NAMES(STRING)
#undef STRING

#if DEBUG
#define DROP_TABLE(str) db.run(String("DROP TABLE ") << str << String(";"))
#else
#define DROP_TABLE(str)
#endif

String const txDefaultMektonName("");
String const txNone             ("ERROR");

String const txMektonTable       ("MektonTable");
String const txHandheldTable     ("HandheldTable");
String const txServoTable        ("ServoTable");
String const txClipTable         ("ClipTable");
String const txEquipmentTable    ("EquipmentTable");
String const txEquipmentInfoTable("EquipmentInfoTable");
String const txLinkLocationTable ("LinkLocationTable");
String const txLinkWeaponTable   ("LinkWeaponTable");

String const txAP           ("AP");
String const txArmor        ("Armor");
String const txBonusMA      ("BonusMA");
String const txBV           ("BV");
String const txComm         ("Comm");
String const txConfiguration("Configuration");
String const txCost         ("Cost");
String const txCrew         ("Crew");
String const txDA           ("DA");
String const txDamage       ("Damage");
String const txFlightMA     ("FlightMA");
String const txFuel         ("Fuel");
String const txGES          ("GES");
String const txGrade        ("Grade");
String const txId           ("Id");
String const txIdEquipment  ("IdEquipment");
String const txIdLink       ("IdLink");
String const txIdServo      ("IdServo");
String const txKills        ("Kills");
String const txManipulation ("Manipulation");
String const txMAPenalty    ("MAPenalty");
String const txMektonName   ("MektonName");
String const txName         ("Name");
String const txPowerplant   ("Powerplant");
String const txRange        ("Range");
String const txShots        ("Shots");
String const txSP           ("SP");
String const txSpace        ("Space");
String const txThrusters    ("Thrusters");
String const txType         ("Type");
String const txWA           ("WA");
String const txWorking      ("Working");

internal inline void
setupMektonTables(Database &db) {
  DROP_TABLE(txMektonTable);
  String query;
  query << "CREATE TABLE IF NOT EXISTS " << txMektonTable << " (";
  query <<   "'" << txName          << "' TEXT    NOT NULL DEFAULT '" << txDefaultMektonName << "',";
  query <<   "'" << txPowerplant    << "' TEXT    NOT NULL DEFAULT 'Cool',";
  query <<   "'" << txConfiguration << "' TEXT    NOT NULL DEFAULT 'Mekton',";
  query <<   "'" << txBonusMA       << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txFlightMA      << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txThrusters     << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txGES           << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txFuel          << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txWorking       << "' BOOL    NOT NULL DEFAULT 0,";
  query <<   "PRIMARY KEY(" << txName << ")";
  query << ") WITHOUT ROWID";
  db.run(query);
  query.clear();

  DROP_TABLE(txServoTable);
  query << "CREATE TABLE IF NOT EXISTS " << txServoTable << " (";
  query <<   "'" << txId         << "' INTEGER NOT NULL,";
  query <<   "'" << txMektonName << "' TEXT    NOT NULL,";
  query <<   "'" << txType       << "' INTEGER NOT NULL DEFAULT 1,";
  query <<   "'" << txGrade      << "' INTEGER NOT NULL DEFAULT 1,";
  query <<   "'" << txArmor      << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txThrusters  << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "'" << txGES        << "' INTEGER NOT NULL DEFAULT 0,";
  query <<   "PRIMARY KEY(" << txId << ")";
  query << ")";
  db.run(query);
  query.clear();

  DROP_TABLE(txHandheldTable);
  query << "CREATE TABLE IF NOT EXISTS " << txHandheldTable << " (";
  query <<   "'" << txMektonName  << "' TEXT    NOT NULL,";
  query <<   "'" << txIdEquipment << "' INTEGER NOT NULL";
  query << ")";
  db.run(query);
  query.clear();

  DROP_TABLE(txClipTable);
  query << "CREATE TABLE IF NOT EXISTS " << txClipTable << " (";
  query <<   "'" << txIdServo     << "' INTEGER NOT NULL,";
  query <<   "'" << txIdEquipment << "' INTEGER NOT NULL";
  query << ")";
  db.run(query);
  query.clear();

  DROP_TABLE(txLinkLocationTable);
  query << "CREATE TABLE IF NOT EXISTS " << txLinkLocationTable << " (";
  query <<   "'" << txId      << "' INTEGER NOT NULL,";
  query <<   "'" << txIdServo << "' INTEGER NOT NULL,";
  query <<   "PRIMARY KEY(" << txId << ")";
  query << ")";
  db.run(query);
  query.clear();

  DROP_TABLE(txLinkWeaponTable);
  query << "CREATE TABLE IF NOT EXISTS " << txLinkWeaponTable << " (";
  query <<   "'" << txIdLink      << "' INTEGER NOT NULL,";
  query <<   "'" << txIdEquipment << "' INTEGER NOT NULL";
  query << ")";
  db.run(query);
  query.clear();

  DROP_TABLE(txEquipmentTable);
  query << "CREATE TABLE IF NOT EXISTS " << txEquipmentTable << " (";
  query <<   "'" << txIdServo          << "' INTEGER NOT NULL,";
  query <<   "'" << txIdEquipment      << "' INTEGER NOT NULL";
  query << ")";
  db.run(query);
  query.clear();

  DROP_TABLE(txEquipmentInfoTable);
  query << "CREATE TABLE IF NOT EXISTS " << txEquipmentInfoTable << " (\n";
  query << "'" << txId           << "' INTEGER NOT NULL UNIQUE,\n";
  query << "'" << txType         << "' INTEGER NOT NULL,\n";
  query << "'" << txCost         << "' REAL    NOT NULL,\n";
  query << "'" << txSpace        << "' INTEGER,\n";
  query << "'" << txKills        << "' INTEGER,\n";
  query << "'" << txDamage       << "' INTEGER,\n";
  query << "'" << txRange        << "' INTEGER,\n";
  query << "'" << txWA           << "' INTEGER,\n";
  query << "'" << txBV           << "' TEXT,\n";
  query << "'" << txShots        << "' TEXT,\n";
  query << "'" << txAP           << "' INTEGER,\n";
  query << "'" << txManipulation << "' BOOL,\n";
  query << "'" << txMAPenalty    << "' INTEGER,\n";
  query << "'" << txCrew         << "' INTEGER,\n";
  query << "'" << txComm         << "' INTEGER,\n";
  query << "'" << txDA           << "' INTEGER,\n";
  query << "'" << txSP           << "' INTEGER,\n";
  query << "PRIMARY KEY(" << txId << ")\n";
  query << ")";
  db.run(query);
  query.clear();

  query << "INSERT INTO " << txEquipmentInfoTable << " VALUES\n";
//                                Id                                                   Type              Cost   Space Kills Damage Range   WA     BV        Shots    AP Manip MAPen  Crew  Comm    DA    SP
  query << "(" << (u32)Equipment::LightBeamGun         << ", '" << (u32)EquipmentType::Beam << "',          2,      1,    1,    1,    4,    1,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::MediumBeamGun        << ", '" << (u32)EquipmentType::Beam << "',          5,      4,    3,    3,    7,    1,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::HeavyBeamGun         << ", '" << (u32)EquipmentType::Beam << "',          9,      9,    6,    6,   10,    1,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::BeamCannon           << ", '" << (u32)EquipmentType::Beam << "',          9,      9,    4,    4,    8,    2,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::HeavyBeamCannon      << ", '" << (u32)EquipmentType::Beam << "',         16,     15,    8,    8,    8,    2,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::NovaCannon           << ", '" << (u32)EquipmentType::Beam << "',         29,     10,    1,   15,   15,    1,  NULL,         '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::PulseCannon          << ", '" << (u32)EquipmentType::Beam << "',         20,     11,    4,    4,    8,    0,   '6',    '1 turn', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::BeamSweeper          << ", '" << (u32)EquipmentType::Beam << "',         15,      6,    2,    2,    4,   -1, 'inf',       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::LightCannon          << ", '" << (u32)EquipmentType::Projectile << "',    3,      3,    3,    3,    5,    0,  NULL,        '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::MediumCannon         << ", '" << (u32)EquipmentType::Projectile << "',    6,      6,    6,    6,    7,    0,  NULL,        '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::HeavyCannon          << ", '" << (u32)EquipmentType::Projectile << "',    9,      9,    9,    9,    9,    0,  NULL,        '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::GiantCannon          << ", '" << (u32)EquipmentType::Projectile << "',   15,     15,   12,   12,   17,    0,  NULL,        '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Autocannon           << ", '" << (u32)EquipmentType::Projectile << "',    6,      5,    2,    2,    4,   -2,   '8', '10 Bursts', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::HeavyAutocannon      << ", '" << (u32)EquipmentType::Projectile << "',   13,     10,    6,    6,    7,   -1,   '4', '10 Bursts', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::EpoxyGun             << ", '" << (u32)EquipmentType::Projectile << "',   12,      7,    6,   18,    5,    2,  NULL,         '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::RocketPod            << ", '" << (u32)EquipmentType::Missile << "',       4,      4,    3,    2,    5,   -1,  NULL,        '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::RocketLauncher       << ", '" << (u32)EquipmentType::Missile << "',       4,      4,    3,    4,    7,    0,  NULL,        '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::MissilePod           << ", '" << (u32)EquipmentType::Missile << "',       5,      5,    2,    6,   13,    1,  NULL,         '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::HeavyMissile         << ", '" << (u32)EquipmentType::Missile << "',       3,      3,    1,   12,   24,    2,  NULL,         '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Sword                << ", '" << (u32)EquipmentType::Melee << "',         4,      4,    5,    5,    1,    1,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Axe                  << ", '" << (u32)EquipmentType::Melee << "',         3,      3,    6,    6,    1,    0,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Mace                 << ", '" << (u32)EquipmentType::Melee << "',         4,      4,    8,    8,    1,    0,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Drill                << ", '" << (u32)EquipmentType::Melee << "',         3,      3,    4,    4,    1,   -1,  NULL,       'inf',    1, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Saw                  << ", '" << (u32)EquipmentType::Melee << "',         5,      5,    6,    6,    1,   -1,  NULL,       'inf',    1, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::ShockWhip            << ", '" << (u32)EquipmentType::Melee << "',         4,      4,    2,    2,    1,   -2,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::EnergySword          << ", '" << (u32)EquipmentType::EnergyMelee << "',   6,      6,    2,    6,    1,    1,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::EnergyAxe            << ", '" << (u32)EquipmentType::EnergyMelee << "',   6,      6,    2,    7,    1,    0,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::EnergyLance          << ", '" << (u32)EquipmentType::EnergyMelee << "',  14,      8,    2,    8,    1,    2,  NULL,       'inf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::NovaSword            << ", '" << (u32)EquipmentType::EnergyMelee << "',  14,      8,    4,   15,    1,    3,  NULL,         '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Hand                 << ", '" << (u32)EquipmentType::ArmExtremity << "',  2,      1,    1,    1, NULL, NULL,  NULL,        NULL, NULL,    1, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::ArmClaw              << ", '" << (u32)EquipmentType::ArmExtremity << "',  4,      1,    2,    2, NULL, NULL,  NULL,        NULL, NULL,    1, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::ArmTalon             << ", '" << (u32)EquipmentType::ArmExtremity << "',  1,      1,    2,    2, NULL, NULL,  NULL,        NULL, NULL,    0, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Pincer               << ", '" << (u32)EquipmentType::ArmExtremity << "',  2,      1,    3,    3, NULL, NULL,  NULL,        NULL, NULL,    0, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Foot                 << ", '" << (u32)EquipmentType::LegExtremity << "',  0,      0,    0,    2, NULL, NULL,  NULL,        NULL, NULL, NULL,    0, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::LegClaw              << ", '" << (u32)EquipmentType::LegExtremity << "',  1,      0,    1,    3, NULL, NULL,  NULL,        NULL, NULL, NULL,   -1, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::LegTalon             << ", '" << (u32)EquipmentType::LegExtremity << "',  2,      0,    3,    5, NULL, NULL,  NULL,        NULL, NULL, NULL,   -2, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::MainCockpit          << ", '" << (u32)EquipmentType::Cockpit << "',       0,      1, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL,    1, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::PassengerSpace       << ", '" << (u32)EquipmentType::Cockpit << "',       1,      1, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL,    1, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::EjectionSeat         << ", '" << (u32)EquipmentType::Cockpit << "',       1,      1, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL,    1, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::EnvironmentPod       << ", '" << (u32)EquipmentType::Cockpit << "',       2,      2, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL,    1, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::MainSensor           << ", '" << (u32)EquipmentType::Sensor << "',        4,      1,    2, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL,    7, 1000, NULL, NULL),\n";
  query << "(" << (u32)Equipment::BackupSensor         << ", '" << (u32)EquipmentType::Sensor << "',        2,      2,    2, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL,    1,  300, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Small                << ", '" << (u32)EquipmentType::Shield << "',        7.5, NULL, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL,   -1,    6),\n";
  query << "(" << (u32)Equipment::Medium               << ", '" << (u32)EquipmentType::Shield << "',        9,   NULL, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL,   -2,    9),\n";
  query << "(" << (u32)Equipment::Large                << ", '" << (u32)EquipmentType::Shield << "',       18,   NULL, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL,    0,   12),\n";
  query << "(" << (u32)Equipment::AntiTheftCodeLock    << ", '" << (u32)EquipmentType::Assembly << "',      0.2,    0, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::AntiTheftCodeAlarm   << ", '" << (u32)EquipmentType::Assembly << "',      0.4,    0, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::DamageControlPackage << ", '" << (u32)EquipmentType::Assembly << "',      1.0,    1, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Spotlights           << ", '" << (u32)EquipmentType::Assembly << "',      0.2,    0, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Liftwire             << ", '" << (u32)EquipmentType::Assembly << "',      0.3,    0, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::Micromanipulators    << ", '" << (u32)EquipmentType::Assembly << "',      1.0,    1, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::StereoSystem         << ", '" << (u32)EquipmentType::Assembly << "',      0.1,    0, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::StorageModule        << ", '" << (u32)EquipmentType::Assembly << "',      1.0,    1, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),\n";
  query << "(" << (u32)Equipment::WeaponLink           << ", '" << (u32)EquipmentType::Assembly << "',      1.0,    0, NULL, NULL, NULL, NULL,  NULL,        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";
  db.run(query);
  query.clear();
}

#define SQL_Eq(property,name)    property << " = " << name
#define SQL_EqStr(property,name) property << " = '" << name << "'"
#define SQL_In(property,select)  property << " IN (" << select << ")"
#define SQL_Column(table,column) table << "." << column

#define SQL_Select(table,column)    String("SELECT ") << SQL_Column(table,column) << " FROM " << table
#define SQL_SelectSum(table,column) String("SELECT SUM(") << SQL_Column(table,column) << ") FROM " << table
#define SQL_Update(table,value)     String("UPDATE ") << table << " SET " << value
#define SQL_Insert(table)           String("INSERT INTO ") << table
#define SQL_Where(expr)             String(" WHERE ") << expr
#define SQL_And(expr)               String(" AND ") << expr

internal
Database db;

namespace mekton {
  /* Utility Declarations */

  String const &toString(Form form);
  String const &toString(ServoType servo_type);
  String const &toString(Grade grade);
  String const &toString(Powerplant powerplant);
  String const &toString(Equipment equipment);
  String const &toString(EquipmentType equipment_type);

  void setupDatabase(Database &database);

  /* Mekton Declarations */

  String *workingMekton(void);
  b32     workingMekton(String const &name);
  b32     addMekton    (String const &name);

  r32           mektonWeight       (String const &mech);
  r32           mektonCost         (String const &mech);
  r32           mektonMV           (String const &mech);
  s32           mektonLandMA       (String const &mech);
  b32           mektonConfiguration(String const &mech, Form form);
  String const &mektonConfiguration(String const &mech);
  b32           mektonPowerplant   (String const &mech, Powerplant powerplant);
  String const &mektonPowerplant   (String const &mech);
  b32           mektonBonusMA      (String const &mech, u32 bonus_ma);
  s32           mektonBonusMA      (String const &mech);
  b32           mektonFlightMA     (String const &mech, u32 flight_ma);
  s32           mektonFlightMA     (String const &mech);
  b32           mektonFuel         (String const &mech, u32 fuel);
  s32           mektonFuel         (String const &mech);
  b32           mektonThrusters    (String const &mech, u32 num_thrusters);
  s32           mektonThrusters    (String const &mech);
  b32           mektonGES          (String const &mech, u32 num_ges);
  s32           mektonGES          (String const &mech);

  b32 addMektonHandheld(String const &mech, Equipment weapon);
  s32 addMektonServo   (String const &mech);

  List<s32> *mektonServoList  (String const &mech);
  r32        mektonServoWeight(String const &mech);
  r32        mektonServoCost  (String const &mech);

  /* Servo Declarations */

  r32           servoWeight     (u32 servo_id);
  r32           servoCost       (u32 servo_id);
  u32           servoKills      (u32 servo_id);
  u32           servoSpace      (u32 servo_id);
  r32           servoGradeWeight(u32 servo_id);
  r32           servoArmorWeight(u32 servo_id);
  s32           servoArmorCost  (u32 servo_id);
  s32           servoArmorSP    (u32 servo_id);
  b32           servoType       (u32 servo_id, ServoType type);
  String const &servoType       (u32 servo_id);
  b32           servoGrade      (u32 servo_id, Grade grade);
  String const &servoGrade      (u32 servo_id);
  b32           servoArmor      (u32 servo_id, Grade armor);
  String const &servoArmor      (u32 servo_id);
  b32           servoThrusters  (u32 servo_id, u32 thrusters);
  s32           servoThrusters  (u32 servo_id);
  b32           servoGES        (u32 servo_id, u32 ges);
  s32           servoGES        (u32 servo_id);

  s32 addServoEquipment(u32 servo_id, Equipment equipment);
  s32 addServoClip     (u32 servo_id, Equipment equipment);
  s32 addServoLink     (u32 servo_id);

  List<s32> *servoEquipmentList(u32 servo_id);

  r32 servoEquipmentWeight(u32 servo_id);
  r32 servoEquipmentCost  (u32 servo_id);
  r32 servoEquipmentSpace (u32 servo_id);

  /* Equipment Declarations */

  b32           equipment            (u32 equipment_id, Equipment equipment);
  r32           equipmentWeight      (u32 equipment_id);
  r32           equipmentCost        (u32 equipment_id);
  r32           equipmentSpace       (u32 equipment_id);
  u32           equipmentTypeId      (u32 equipment_id);
  String const &equipmentName        (u32 equipment_id);
  u32           equipmentKills       (u32 equipment_id);
  u32           equipmentDamage      (u32 equipment_id);
  u32           equipmentRange       (u32 equipment_id);
  s32           equipmentWA          (u32 equipment_id);
  u32           equipmentShots       (u32 equipment_id);
  u32           equipmentBV          (u32 equipment_id);
  u32           equipmentAP          (u32 equipment_id);
  u32           equipmentManipulation(u32 equipment_id);
  u32           equipmentMAPenalty   (u32 equipment_id);
  u32           equipmentCrew        (u32 equipment_id);
  u32           equipmentComm        (u32 equipment_id);
  u32           equipmentDA          (u32 equipment_id);
  u32           equipmentSP          (u32 equipment_id);

  /* Weapon Link Declarations */

  s32 addLinkWeapon(u32 link_id, u32 equipment_id);


  /* Utility Implementations */

  String const &toString(Form form) {
    switch (form) {
      case Form::Mekton:       return txMekton;
      case Form::Mechabeast:   return txMechabeast;
      case Form::Mechatank:    return txMechatank;
      case Form::Mechafighter: return txMechafighter;
      default: return txNone;
    }
  }

  String const &toString(ServoType servo_type) {
    switch (servo_type) {
      case ServoType::Torso: return txTorso;
      case ServoType::Arm:   return txArm;
      case ServoType::Leg:   return txLeg;
      case ServoType::Head:  return txHead;
      case ServoType::Wing:  return txWing;
      case ServoType::Tail:  return txTail;
      case ServoType::Pod:   return txPod;
      default: return txNone;
    }
  }

  String const &toString(Grade grade) {
    switch (grade) {
      case Grade::SuperLight:    return txSuperLight;
      case Grade::LightWeight:   return txLightWeight;
      case Grade::Striker:       return txStriker;
      case Grade::MediumStriker: return txMediumStriker;
      case Grade::HeavyStriker:  return txHeavyStriker;
      case Grade::MediumWeight:  return txMediumWeight;
      case Grade::LightHeavy:    return txLightHeavy;
      case Grade::MediumHeavy:   return txMediumHeavy;
      case Grade::ArmoredHeavy:  return txArmoredHeavy;
      case Grade::SuperHeavy:    return txSuperHeavy;
      case Grade::MegaHeavy:     return txMegaHeavy;
      default: return txNone;
    }
  }

  String const &toString(Powerplant powerplant) {
    switch (powerplant) {
      case Powerplant::Cool: return txCool;
      case Powerplant::Hot:  return txHot;
      default: return txNone;
    }
  }

  String const &toString(Equipment equipment) {
    switch (equipment) {
      // Weapons
      case Equipment::LightBeamGun:         return txLightBeamGun;
      case Equipment::MediumBeamGun:        return txMediumBeamGun;
      case Equipment::HeavyBeamGun:         return txHeavyBeamGun;
      case Equipment::BeamCannon:           return txBeamCannon;
      case Equipment::HeavyBeamCannon:      return txHeavyBeamCannon;
      case Equipment::NovaCannon:           return txNovaCannon;
      case Equipment::PulseCannon:          return txPulseCannon;
      case Equipment::BeamSweeper:          return txBeamSweeper;
      case Equipment::LightCannon:          return txLightCannon;
      case Equipment::MediumCannon:         return txMediumCannon;
      case Equipment::HeavyCannon:          return txHeavyCannon;
      case Equipment::GiantCannon:          return txGiantCannon;
      case Equipment::Autocannon:           return txAutocannon;
      case Equipment::HeavyAutocannon:      return txHeavyAutocannon;
      case Equipment::EpoxyGun:             return txEpoxyGun;
      case Equipment::RocketPod:            return txRocketPod;
      case Equipment::RocketLauncher:       return txRocketLauncher;
      case Equipment::MissilePod:           return txMissilePod;
      case Equipment::HeavyMissile:         return txHeavyMissile;
      case Equipment::Sword:                return txSword;
      case Equipment::Axe:                  return txAxe;
      case Equipment::Mace:                 return txMace;
      case Equipment::Drill:                return txDrill;
      case Equipment::Saw:                  return txSaw;
      case Equipment::ShockWhip:            return txShockWhip;
      case Equipment::EnergySword:          return txEnergySword;
      case Equipment::EnergyAxe:            return txEnergyAxe;
      case Equipment::EnergyLance:          return txEnergyLance;
      case Equipment::NovaSword:            return txNovaSword;
      // Arm Extremity
      case Equipment::Hand:                 return txHand;
      case Equipment::ArmClaw:              return txArmClaw;
      case Equipment::ArmTalon:             return txArmTalon;
      case Equipment::Pincer:               return txPincer;
      // Leg Extremity
      case Equipment::Foot:                 return txFoot;
      case Equipment::LegClaw:              return txLegClaw;
      case Equipment::LegTalon:             return txLegTalon;
      // Cockpit
      case Equipment::MainCockpit:          return txMainCockpit;
      case Equipment::PassengerSpace:       return txPassengerSpace;
      case Equipment::EjectionSeat:         return txEjectionSeat;
      case Equipment::EnvironmentPod:       return txEnvironmentPod;
      // Sensor
      case Equipment::MainSensor:           return txMainSensor;
      case Equipment::BackupSensor:         return txBackupSensor;
      // Shield
      case Equipment::Small:                return txSmall;
      case Equipment::Medium:               return txMedium;
      case Equipment::Large:                return txLarge;
      // Assembly
      case Equipment::AntiTheftCodeLock:    return txAntiTheftCodeLock;
      case Equipment::AntiTheftCodeAlarm:   return txAntiTheftCodeAlarm;
      case Equipment::DamageControlPackage: return txDamageControlPackage;
      case Equipment::Spotlights:           return txSpotlights;
      case Equipment::Liftwire:             return txLiftwire;
      case Equipment::Micromanipulators:    return txMicromanipulators;
      case Equipment::StereoSystem:         return txStereoSystem;
      case Equipment::StorageModule:        return txStorageModule;
      case Equipment::WeaponLink:           return txWeaponLink;
      default: return txNone;
    }
  }

  String const &toString(EquipmentType equipment_type) {
    switch (equipment_type) {
      case EquipmentType::Beam:         return txBeam;
      case EquipmentType::Projectile:   return txProjectile;
      case EquipmentType::Missile:      return txMissile;
      case EquipmentType::Melee:        return txMelee;
      case EquipmentType::EnergyMelee:  return txEnergyMelee;
      case EquipmentType::ArmExtremity: return txArmExtremity;
      case EquipmentType::LegExtremity: return txLegExtremity;
      case EquipmentType::Cockpit:      return txCockpit;
      case EquipmentType::Sensor:       return txSensor;
      case EquipmentType::Shield:       return txShield;
      case EquipmentType::Assembly:     return txAssembly;
      default: return txNone;
    }
  }

  void setupDatabase(Database &database) {
    db = database;
    setupMektonTables(db);
  }

  /* Mekton Implementations */

  String *workingMekton() {
    String *name = db.processString(SQL_Select(txMektonTable, txName) << SQL_Where(SQL_Eq(txWorking, true)));

    if (!name) {
      db.run(String("INSERT INTO ") << txMektonTable << " DEFAULT VALUES");
      name = new String(txDefaultMektonName);
    }

    return name;
  }

  b32 workingMekton(String const &name) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txWorking, false)))
      && db.run(SQL_Update(txMektonTable, SQL_Eq(txWorking, true)) << SQL_Where(SQL_EqStr(txName, name)));
  }

  b32 addMekton(String const &name) {
    // Check if this mekton already exists
    switch (db.processInt(SQL_Select(txMektonTable, txWorking) << SQL_Where(SQL_EqStr(txName, name)))) {
      // Exists, but not the working mekton
      case 0:  return workingMekton(name);
      // Exists, and is the working mekton
      case 1:  return true;
      // Does not exist
      default: return db.run(SQL_Insert(txMektonTable) << " DEFAULT VALUES")
        && db.run(SQL_Update(txMektonTable, SQL_EqStr(txName, name)))
        && workingMekton(name);
    }
  }

  r32 mektonWeight(String const &mech) {
    r32 servo_weight = mektonServoWeight(mech);
    return servo_weight;
  }

  r32 mektonCost(String const &mech) {
    r32 servo_cost = mektonServoCost(mech);
    r32 flight_ma_cost = mektonWeight(mech) * 0.0375 * mektonFlightMA(mech);
    r32 bonus_ma_cost = mektonWeight(mech) * 0.025 * mektonBonusMA(mech);
    return servo_cost + flight_ma_cost + bonus_ma_cost;
  }

  r32 mektonMV(String const &mech) {
    r32 mv = -mektonWeight(mech) * 0.1f;
    return (mv < -10) ? -10 : mv;
  }

  s32 mektonLandMA(String const &mech) {
    s32 wt = 6 - (s32)(mektonWeight(mech) * 0.05f);
    return (wt < 2 ? 2 : wt);
  }

  b32 mektonConfiguration(String const &mech, Form form) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txConfiguration, (s32)form)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  String const &mektonConfiguration(String const &mech) {
    return toString((Form)db.processInt(SQL_Select(txMektonTable, txConfiguration) << SQL_Where(SQL_EqStr(txName, mech))));
  }

  b32 mektonPowerplant(String const &mech, Powerplant powerplant) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txPowerplant, (s32)powerplant)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  String const &mektonPowerplant(String const &mech) {
    return toString((Powerplant)db.processInt(SQL_Select(txMektonTable, txPowerplant) << SQL_Where(SQL_EqStr(txName, mech))));
  }

  b32 mektonBonusMA(String const &mech, u32 bonus_ma) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txBonusMA, bonus_ma)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  s32 mektonBonusMA(String const &mech) {
    return db.processInt(SQL_Select(txMektonTable, txBonusMA) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  b32 mektonFlightMA(String const &mech, u32 flight_ma) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txFlightMA, flight_ma)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  s32 mektonFlightMA(String const &mech) {
    return db.processInt(SQL_Select(txMektonTable, txFlightMA) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  b32 mektonFuel(String const &mech, u32 fuel) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txFuel, fuel)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  s32 mektonFuel(String const &mech) {
    return db.processInt(SQL_Select(txMektonTable, txFuel) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  b32 mektonThrusters(String const &mech, u32 num_thrusters) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txThrusters, num_thrusters)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  s32 mektonThrusters(String const &mech) {
    return db.processInt(SQL_Select(txMektonTable, txThrusters) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  b32 mektonGES(String const &mech, u32 num_ges) {
    return db.run(SQL_Update(txMektonTable, SQL_Eq(txGES, num_ges)) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  s32 mektonGES(String const &mech) {
    return db.processInt(SQL_Select(txMektonTable, txGES) << SQL_Where(SQL_EqStr(txName, mech)));
  }

  b32 addMektonHandheld(String const &mech, Equipment weapon) {
    return db.run(SQL_Insert(txHandheldTable) << " VALUES ('" << mech << "'," << (s32)weapon << ")");
  }

  s32 addMektonServo(String const &mech) {
    if (db.run(SQL_Insert(txServoTable) << "(" << txMektonName << ") VALUES ('" << mech << "')"))
      return db.lastId();
    else
      return -1;
  }

  List<s32> *mektonServoList(String const &mech) {
    return db.processIntColumn(SQL_Select(txServoTable, txId) << SQL_Where(SQL_EqStr(txMektonName, mech)));
  }

  r32 mektonServoWeight(String const &mech) {
    List<s32> *servos = mektonServoList(mech);
    r32 total = 0;
    ForEach (*servos)
      total += servoWeight(*it);
    delete servos;
    return total;
  }

  r32 mektonServoCost(String const &mech) {
    List<s32> *servos = mektonServoList(mech);
    r32 total = 0;
    ForEach (*servos)
      total += servoCost(*it);
    delete servos;
    return total;
  }

  /* Servo Implementations */

  r32 servoWeight(u32 servo_id) {
    r32 total = 0;
    total += servoGradeWeight(servo_id);
    total += servoArmorWeight(servo_id);
    total += servoEquipmentWeight(servo_id);
    total += servoThrusters(servo_id);
    total += servoGES(servo_id);
    return total;
  }

  r32 servoCost(u32 servo_id) {
    String query;
    query << "SELECT TOTAL(\n";
    query << "  CASE\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Torso << ") THEN " << txGrade << " * 2\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Arm << "," << (u32)ServoType::Leg << ") THEN " << txGrade << " + 1\n";
    query << "  ELSE " << txGrade << "\n";
    query << "  END)\n";
    query << "FROM " << txServoTable << SQL_Where(SQL_Eq(txId, servo_id));
    return db.processReal(query) + servoThrusters(servo_id) + servoGES(servo_id);
  }

  u32 servoKills(u32 servo_id) {
    String query;
    query << "SELECT TOTAL(\n";
    query << "  CASE\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Torso << ") THEN " << txGrade << " * 2\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Arm << "," << (u32)ServoType::Leg << ") THEN " << txGrade << " + 1\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Pod << ") THEN 0\n";
    query << "  ELSE " << txGrade << "\n";
    query << "  END)\n";
    query << "FROM " << txServoTable << SQL_Where(SQL_Eq(txId, servo_id));
    return db.processInt(query);
  }

  u32 servoSpace(u32 servo_id) {
    String query;
    query << "SELECT TOTAL(\n";
    query << "  CASE\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Torso << "," << (u32)ServoType::Pod << ") THEN " << txGrade << " * 2\n";
    query << "    WHEN " << txType << " IN (" << (u32)ServoType::Arm << "," << (u32)ServoType::Leg << ") THEN " << txGrade << " + 1\n";
    query << "  ELSE " << txGrade << "\n";
    query << "  END)\n";
    query << "FROM " << txServoTable << SQL_Where(SQL_Eq(txId, servo_id));
    return db.processInt(query);
  }

  r32 servoGradeWeight(u32 servo_id) {
    return servoKills(servo_id) * 0.5f;
  }

  r32 servoArmorWeight(u32 servo_id) {
    return servoArmorCost(servo_id) * 0.5f;
  }

  s32 servoArmorCost(u32 servo_id) {
    return db.processInt(SQL_Select(txServoTable, txArmor) << SQL_Where(SQL_EqStr(txId, servo_id)));
  }

  s32 servoArmorSP(u32 servo_id) {
    return db.processInt(SQL_Select(txServoTable, txGrade) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  b32 servoType(u32 servo_id, ServoType type) {
    return db.run(SQL_Update(txServoTable, SQL_Eq(txType, (s32)type)) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  String const &servoType(u32 servo_id) {
    return toString((ServoType)db.processInt(SQL_Select(txServoTable, txType) << SQL_Where(SQL_Eq(txId, servo_id))));
  }

  b32 servoGrade(u32 servo_id, Grade grade) {
    return db.run(SQL_Update(txServoTable, SQL_Eq(txGrade, (s32)grade)) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  String const &servoGrade(u32 servo_id) {
    return toString((Grade)db.processInt(SQL_Select(txServoTable, txGrade) << SQL_Where(SQL_Eq(txId, servo_id))));
  }

  b32 servoArmor(u32 servo_id, Grade armor) {
    return db.run(SQL_Update(txServoTable, SQL_Eq(txArmor, (s32)armor)) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  String const &servoArmor(u32 servo_id) {
    return toString((Grade)db.processInt(SQL_Select(txServoTable, txArmor) << SQL_Where(SQL_Eq(txId, servo_id))));
  }

  b32 servoThrusters(u32 servo_id, u32 thrusters) {
    return db.run(SQL_Update(txServoTable, SQL_Eq(txThrusters, thrusters)) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  s32 servoThrusters(u32 servo_id) {
    return db.processInt(SQL_Select(txServoTable, txThrusters) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  b32 servoGES(u32 servo_id, u32 ges) {
    return db.run(SQL_Update(txServoTable, SQL_Eq(txGES, ges)) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  s32 servoGES(u32 servo_id) {
    return db.processInt(SQL_Select(txServoTable, txGES) << SQL_Where(SQL_Eq(txId, servo_id)));
  }

  s32 addServoEquipment(u32 servo_id, Equipment equipment) {
    if (db.run(SQL_Insert(txEquipmentTable) << "(" << txIdServo << "," << txIdEquipment << ") VALUES " <<
               "(" << servo_id << "," << (s32)equipment << ")"))
      return db.lastId();
    else
      return -1;
  }

  s32 addServoClip(u32 servo_id, Equipment equipment) {
    if (db.run(SQL_Insert(txClipTable) << "(" << txIdServo << "," << txIdEquipment << ") VALUES " <<
               "(" << servo_id << "," << (s32)equipment << ")"))
      return db.lastId();
    else
      return -1;
  }

  s32 addServoLink(u32 servo_id) {
    if (db.run(SQL_Insert(txLinkLocationTable) << "(" << txIdServo << ") VALUES (" << servo_id << ")"))
      return db.lastId();
    else
      return -1;
  }

  List<s32> *servoEquipmentList(u32 servo_id) {
    return db.processIntColumn(SQL_Select(txEquipmentTable, "rowid") << SQL_Where(SQL_Eq(txIdServo, servo_id)));
  }

  r32 servoEquipmentWeight(u32 servo_id) {
    String query;
    query << "SELECT TOTAL(" << txKills << ") * 0.5";
    query << " FROM " << txEquipmentInfoTable;
    query << SQL_Where(SQL_In(txId, SQL_Select(txEquipmentTable, txIdEquipment) << SQL_Where(SQL_Eq(txIdServo, servo_id))));
    return db.processReal(query);
  }

  r32 servoEquipmentCost(u32 servo_id) {
    String query;
    query << "SELECT TOTAL(" << txCost << ")";
    query << " FROM " << txEquipmentInfoTable;
    query << SQL_Where(SQL_In(txId, SQL_Select(txEquipmentTable, txIdEquipment) << SQL_Where(SQL_Eq(txIdServo, servo_id))));
    return db.processReal(query);
  }

  r32 servoEquipmentSpace(u32 servo_id) {
    String query;
    query << "SELECT TOTAL(" << txSpace << ")";
    query << " FROM " << txEquipmentInfoTable;
    query << SQL_Where(SQL_In(txId, SQL_Select(txEquipmentTable, txIdEquipment) << SQL_Where(SQL_Eq(txIdServo, servo_id))));
    return db.processReal(query);
  }

  /* Equipment Declarations */
#define IdInEquipmentTable(id) SQL_In(txId, SQL_Select(txEquipmentTable, txIdEquipment) << SQL_Where(SQL_Eq("rowid", id)))

  b32 equipment(u32 equipment_id, Equipment equipment) {
    return db.run(SQL_Update(txEquipmentTable,
                             SQL_Eq(txIdEquipment, (u32)equipment) << SQL_Where(SQL_Eq("rowid", equipment_id))));
  }

  r32 equipmentWeight(u32 equipment_id) {
    return db.processReal(SQL_Select(txEquipmentInfoTable, txKills) <<
                          SQL_Where(IdInEquipmentTable(equipment_id))) * 0.5f;
  }

  r32 equipmentCost(u32 equipment_id) {
    return db.processReal(SQL_Select(txEquipmentInfoTable, txCost) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  r32 equipmentSpace(u32 equipment_id) {
    return db.processReal(SQL_Select(txEquipmentInfoTable, txSpace) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentTypeId(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txType) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  String const &equipmentName(u32 equipment_id) {
    return toString((Equipment)db.processInt(SQL_Select(txEquipmentInfoTable, txId) <<
                                             SQL_Where(IdInEquipmentTable(equipment_id))));
  }

  u32 equipmentKills(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txKills) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentDamage(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txDamage) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentRange(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txRange) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  s32 equipmentWA(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txWA) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentShots(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txShots) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentBV(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txBV) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentAP(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txAP) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentManipulation(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txManipulation) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentMAPenalty(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txMAPenalty) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentCrew(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txCrew) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentComm(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txComm) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentDA(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txDA) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  u32 equipmentSP(u32 equipment_id) {
    return db.processInt(SQL_Select(txEquipmentInfoTable, txSP) <<
                          SQL_Where(IdInEquipmentTable(equipment_id)));
  }

  /* Weapon Link Declarations */

  s32 addLinkWeapon(u32 link_id, u32 equipment_id) {
    if (db.run(SQL_Insert(txLinkWeaponTable) << "(" << txIdLink << "," << txIdEquipment << ") VALUES " <<
               "(" << link_id << "," << equipment_id << ")"))
      return db.lastId();
    else
      return -1;
  }

};

String testRapier() {
  using namespace mekton;

  String name("Rapier");

  String *temp = db.processString(SQL_Select(txMektonTable, txName) << SQL_Where(SQL_EqStr(txName, name)));
  if (temp) {
    delete temp;
    return name;
  }

  addMekton(name);
  mektonConfiguration(name, Form::Mekton);
  mektonPowerplant(name, Powerplant::Cool);
  mektonBonusMA(name, 0);
  mektonFlightMA(name, 9);
  mektonFuel(name, 1000);
  addMektonHandheld(name, Equipment::HeavyAutocannon);

  s32 rapier_head = addMektonServo(name);
  servoType(rapier_head, ServoType::Head);
  servoGrade(rapier_head, Grade::MediumStriker);
  servoArmor(rapier_head, Grade::HeavyStriker);
  servoThrusters(rapier_head, 0);
  addServoEquipment(rapier_head, Equipment::MainSensor);
  addServoEquipment(rapier_head, Equipment::BackupSensor);
  addServoEquipment(rapier_head, Equipment::Spotlights);
  addServoEquipment(rapier_head, Equipment::Spotlights);

  s32 rapier_larm = addMektonServo(name);
  servoType(rapier_larm, ServoType::Arm);
  servoGrade(rapier_larm, Grade::HeavyStriker);
  servoArmor(rapier_larm, Grade::HeavyStriker);
  servoThrusters(rapier_larm, 0);
  addServoEquipment(rapier_larm, Equipment::RocketLauncher);
  addServoEquipment(rapier_larm, Equipment::Micromanipulators);
  addServoEquipment(rapier_larm, Equipment::Hand);

  s32 rapier_rarm = addMektonServo(name);
  servoType(rapier_rarm, ServoType::Arm);
  servoGrade(rapier_rarm, Grade::HeavyStriker);
  servoArmor(rapier_rarm, Grade::HeavyStriker);
  servoThrusters(rapier_rarm, 0);
  addServoEquipment(rapier_rarm, Equipment::RocketLauncher);
  addServoEquipment(rapier_rarm, Equipment::Micromanipulators);
  addServoEquipment(rapier_rarm, Equipment::Hand);

  s32 rapier_lleg = addMektonServo(name);
  servoType(rapier_lleg, ServoType::Leg);
  servoGrade(rapier_lleg, Grade::LightHeavy);
  servoArmor(rapier_lleg, Grade::HeavyStriker);
  servoThrusters(rapier_lleg, 4);
  addServoEquipment(rapier_lleg, Equipment::RocketLauncher);
  addServoClip(rapier_lleg, Equipment::Autocannon);
  addServoEquipment(rapier_lleg, Equipment::Foot);

  s32 rapier_rleg = addMektonServo(name);
  servoType(rapier_rleg, ServoType::Leg);
  servoGrade(rapier_rleg, Grade::LightHeavy);
  servoArmor(rapier_rleg, Grade::HeavyStriker);
  servoThrusters(rapier_rleg, 4);
  addServoEquipment(rapier_rleg, Equipment::RocketLauncher);
  addServoClip(rapier_rleg, Equipment::Autocannon);
  addServoEquipment(rapier_rleg, Equipment::Foot);

  s32 rapier_pod = addMektonServo(name);
  servoType(rapier_pod, ServoType::Pod);
  servoGrade(rapier_pod, Grade::MediumWeight);
  servoArmor(rapier_pod, Grade::HeavyStriker);
  servoThrusters(rapier_pod, 0);
  s32 wep_id1 = addServoEquipment(rapier_pod, Equipment::Autocannon);
  s32 wep_id2 = addServoEquipment(rapier_pod, Equipment::Autocannon);

  s32 rapier_torso = addMektonServo(name);
  servoType(rapier_torso, ServoType::Torso);
  servoGrade(rapier_torso, Grade::MediumWeight);
  servoArmor(rapier_torso, Grade::HeavyStriker);
  servoThrusters(rapier_torso, 12);
  s32 link_torso = addServoLink(rapier_torso);
  addLinkWeapon(link_torso, wep_id1);
  addLinkWeapon(link_torso, wep_id2);
  addServoEquipment(rapier_torso, Equipment::EnvironmentPod);
  addServoEquipment(rapier_torso, Equipment::AntiTheftCodeLock);
  addServoEquipment(rapier_torso, Equipment::Liftwire);

  return name;
}