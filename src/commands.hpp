#pragma once

#include "string.hpp"

#include "mektool_ui.h"
#include "mekton.h"

namespace cmd {
  enum class Type {
    Unknown,
    AddServo,
    AddEquipment,
    ChangeSpinner,
    ClearData,
    ExportData,
    SaveData,
    LoadData,
  };

  struct Command;

  struct Unknown {
    String name;
  };

  struct AddServo {
    u32 rowid;
    b32 is_success;
  };

  struct AddEquipment {
    ServoPanel* panel;
  };

  enum class SpinnerChange {
    Increment,
    Decrement,
  };

  struct ChangeSpinner {
    SpinnerChange type;
    String* value;

    void increment() {
      s64 val = value->toU64() + 1;
      if (val < 100) {
        value->fromU64(val);
      }
    }

    void decrement() {
      s64 val = value->toU64() - 1;
      if (val >= 0) {
        value->fromU64(val);
      }
    }
  };

  struct ClearData {
  };

  struct ExportData {
  };

  struct SaveData {
  };

  struct LoadData {
  };

  struct Command {
    Type type;
    Database* db;

    union {
      Unknown       unknown;
      AddServo      add_servo;
      AddEquipment  add_equipment;
      ChangeSpinner change_spinner;
      ClearData     clear_data;
      ExportData    export_data;
      SaveData      save_data;
      LoadData      load_data;
    };

    void apply() {
      switch (type) {
        case Type::Unknown: {
          printf("Applying unknown  %s.\n", unknown.name.text);
        } break;

        case Type::AddServo: {
          add_servo.is_success = db->runStatement("INSERT INTO Servos DEFAULT VALUES;");
          add_servo.rowid = add_servo.is_success
            ? db->lastId()
            : add_servo.rowid;
        } break;

        case Type::AddEquipment: {
          add_equipment.panel->equip_info.push_back(EquipPanel(Timer::RuntimeInSeconds()));
        } break;

        case Type::ChangeSpinner: {
          switch (change_spinner.type) {
            case SpinnerChange::Increment: change_spinner.increment(); break;
            case SpinnerChange::Decrement: change_spinner.decrement(); break;
          }
        } break;

        case Type::ClearData: {} break;

        case Type::ExportData: {} break;

        case Type::SaveData: {} break;

        case Type::LoadData: {} break;
      }

    error:
      return;
    }

    void unapply() {
      switch (type) {
        case Type::Unknown: {
          printf("Unapplying unknown command %s.\n", unknown.name.text);
        } break;

        case Type::AddServo: {
          if (add_servo.is_success) {
            if (add_servo.rowid > 0) {
              String query;
              query << "DELETE FROM Servos WHERE rowid = " << add_servo.rowid << ";";
              db->runStatement(query);
            }
          }
        } break;

        case Type::AddEquipment: {
          add_equipment.panel->equip_info.pop_back();
        } break;

        case Type::ChangeSpinner: {
          switch (change_spinner.type) {
            case SpinnerChange::Increment: change_spinner.decrement(); break;
            case SpinnerChange::Decrement: change_spinner.increment(); break;
          }
        } break;

        case Type::ClearData: {} break;

        case Type::ExportData: {} break;

        case Type::SaveData: {} break;

        case Type::LoadData: {} break;
      }

    error:
      return;
    }

    ~Command() {}
  };

  internal inline
  Command* create() { return (Command*)calloc(1, sizeof(Command)); }

  Command* createUnknown(char const* name) {
    Command* command = create();
    check_mem(command);

    command->type = Type::Unknown;
    command->unknown.name = String(name);

    return command;
  error:
    return nullptr;
  }

  Command* createAddServo(Database* db) {
    Command* command = create();
    check_mem(command);

    command->type = Type::AddServo;
    command->db = db;

    return command;
  error:
    return nullptr;
  }

  Command* createAddEquipment(ServoPanel* panel) {
    Command* command = create();
    check_mem(command);

    command->type = Type::AddEquipment;
    command->add_equipment.panel = panel;

    return command;
  error:
    return nullptr;
  }

  Command* createChangeSpinner(SpinnerChange type, String* value) {
    Command* command = create();
    check_mem(command);

    command->type = Type::ChangeSpinner;
    command->change_spinner.type = type;
    command->change_spinner.value = value;

    return command;
  error:
    return nullptr;
  }

  Command* createClearData() {
    Command* command = create();
    check_mem(command);

    command->type = Type::ClearData;

    return command;
  error:
    return nullptr;
  }

  Command* createExportData() {
    Command* command = create();
    check_mem(command);

    command->type = Type::ExportData;

    return command;
  error:
    return nullptr;
  }

  Command* createSaveData(Database* db) {
    Command* command = create();
    check_mem(command);

    command->type = Type::SaveData;
    command->db = db;

    return command;
  error:
    return nullptr;
  }

  Command* createLoadData(Database* db) {
    Command* command = create();
    check_mem(command);

    command->type = Type::LoadData;
    command->db = db;

    return command;
  error:
    return nullptr;
  }
}