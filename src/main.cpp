/*
File: 'main.cpp'
Author: Seth Renshaw
Date Created: 7/9/2016
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "SDL_helper.h"
#include "database.hpp"
#include "mekton.hpp"
// #include "app.h"

namespace msgbox {
  b32 showMessageBox(char const *title, char const *message) {
    return SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, title, message, nullptr) < 0;
  }

  b32 showWarningBox(char const *title, char const *message) {
    return SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, title, message, nullptr) < 0;
  }

  void crashWithErrorBox(char const *title, char const *message) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, title, message, nullptr) < 0)
      fprintf(stderr, "ErrorBox Error: %s\n", SDL_GetError());
    assert(false);
  }
}

/* New Tracker Stuff */

namespace tracker {
  static union {
    struct {
      u32 x;
      u32 y;
    };
    SDL_Point position = { 0, 0 };
  };
  SDL_Rect bounds = {};
  b32 held = false;
  b32 triggered = false;
  s32 wheel = 0;

  inline
  b32 inRect(u32 other_x, u32 other_y, u32 other_w, u32 other_h) {
    SDL_Rect area { other_x, other_y, other_w, other_h };
    b32 in_rect = SDL_PointInRect(&position, &area);
    b32 valid_bounds = (bounds.w > 0 && bounds.h > 0);
    b32 in_bounds = SDL_PointInRect(&position, &bounds);
    return in_rect && (valid_bounds ? in_bounds : true);
  }

  inline
  b32 isTriggered(u32 other_x, u32 other_y, u32 other_w, u32 other_h) {
    return triggered && inRect(other_x, other_y, other_w, other_h);
  }

  inline
  b32 isClicked(u32 other_x, u32 other_y, u32 other_w, u32 other_h) {
    return held && inRect(other_x, other_y, other_w, other_h);
  }

  inline
  void setBoundingRect(s32 rx, s32 ry, s32 rw, s32 rh) {
    bounds = { rx, ry, rw, rh };
  }

  inline
  void clearBoundingRect() {
    setBoundingRect(0,0,0,0);
  }

}

/* End Tracker Stuff */

/* New Timer Stuff */

namespace timer {
  r64 curr_time  = 0;
  r64 last_time  = 0;
  r64 delta_time = 0;

  internal
  r64 RuntimeInSeconds() {
    return SDL_GetTicks() * 0.01;
  }

  void update() {
    curr_time  = RuntimeInSeconds();
    delta_time = curr_time - last_time;
    last_time  = curr_time;
  }
}

/* End Timer Stuff */

/* New Renderer Stuff */

namespace render {
  // using namespace msgbox;

  u32 const Black  = 0x000000ff;
  u32 const Blue   = 0x0000ffff;
  u32 const Green  = 0x00ff00ff;
  u32 const Red    = 0xff0000ff;
  u32 const Cyan   = 0x00ffffff;
  u32 const Violet = 0xff00ffff;
  u32 const Orange = 0xffff00ff;
  u32 const White  = 0xffffffff;

  internal b32           initialized;
  internal SDL_Window   *window;
  internal SDL_Renderer *renderer;
  internal TTF_Font     *font;

  // Initialization & Shutdown

  void close() {
    if (font)
      TTF_CloseFont(font);
    if (renderer)
      SDL_DestroyRenderer(renderer);
    if (window)
      SDL_DestroyWindow(window);
    if (TTF_WasInit())
      TTF_Quit();
    if (SDL_WasInit(SDL_INIT_VIDEO) != 0)
      SDL_QuitSubSystem(SDL_INIT_VIDEO);
  }

  void showError(String const &title, String const &message, String const &error) {
    String msg(message);
    msg << "\nError: " << error;
    close();
    msgbox::crashWithErrorBox(title.text, msg.text);
  }

  b32 init(u32 window_width, u32 window_height) {
    if (initialized) {
      msgbox::showWarningBox("Window", "Window already initialized.");
      return false;
    }

    if (SDL_InitSubSystem(SDL_INIT_VIDEO) < 0)
      showError("Window", "Video subsystem could not be initialized.", SDL_GetError());

    if (TTF_Init() < 0)
      showError("Window", "Font system could not be initialized.", TTF_GetError());

    window = SDL_CreateWindow("Window Title",
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              window_width, window_height,
                              SDL_WINDOW_BORDERLESS);
    if (!window)
      showError("Window", "Window could not be initialized.", SDL_GetError());

    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
    if (!renderer)
      showError("Window", "Render surface could not be initialized.", SDL_GetError());

    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    // SDL_RWops *font_file = SDL_RWFromFile("zekton rg.ttf", "rb");
    SDL_RWops *font_file = SDL_RWFromFile("Ubuntu-R.ttf", "rb");
    if (!font_file)
      showError("Window", "Cannot find the font file.", SDL_GetError());

    font = TTF_OpenFontRW(font_file, 1, 12);
    if (!font)
      showError("Window", "Cannot open the font file.", TTF_GetError());

    initialized = true;
    return true;
  }


  /* Window Options */

  void setTitle(char const *title) {
    SDL_SetWindowTitle(window, title);
  }

  void enableBorder(b32 enabled) {
    SDL_SetWindowBordered(window, enabled ? SDL_TRUE : SDL_FALSE);
  }

  void showWindow(b32 enabled) {
    if (enabled)
      SDL_ShowWindow(window);
    else
      SDL_HideWindow(window);
  }

  void captureInput(b32 enabled) {
    SDL_SetWindowGrab(window, enabled ? SDL_TRUE : SDL_FALSE);
  }


  /* Runtime Functions */

  void clear() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
  }

  void present() {
    SDL_RenderPresent(renderer);
    // SDL_Delay(10);
  }

  void setColor(u32 color) {
    SDL_SetRenderDrawColor(renderer,
                           (color >> 24) & 0xFF,
                           (color >> 16) & 0xFF,
                           (color >>  8) & 0xFF,
                           (color >>  0) & 0xFF);
  }

  u32 getColor() {
    SDL_Color color;
    SDL_GetRenderDrawColor(renderer, &color.r, &color.g, &color.b, &color.a);
    return (color.r << 24) + (color.g << 16) + (color.b << 8) + color.a;
  }

  void setClipRect(u32 x, u32 y, u32 w, u32 h) {
    SDL_Rect area = { x, y, w, h };
    SDL_RenderSetClipRect(renderer, &area);
  }

  void clearClipRect() {
    SDL_RenderSetClipRect(renderer, nullptr);
  }

  SDL_Texture *createText(String const &txt) {
    u32 color = getColor();
    SDL_Color sdl_color = {
      (color >> 24) & 0xff,
      (color >> 16) & 0xff,
      (color >>  8) & 0xff,
      (color >>  0) & 0xff,
    };
    SDL_Surface *text_surface = TTF_RenderUTF8_Blended(font, txt.text, sdl_color);
    SDL_Texture *text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
    SDL_FreeSurface(text_surface);
    return text_texture;
  }

  SDL_Point textSize(SDL_Texture *texture) {
    SDL_Point size;
    SDL_QueryTexture(texture, nullptr, nullptr, &size.x, &size.y);
    return size;
  }

  void text(String const &txt, u32 x, u32 y) {
    SDL_Texture *text_texture = createText(txt);
    SDL_Rect text_area = { x, y };
    SDL_QueryTexture(text_texture, nullptr, nullptr, &text_area.w, &text_area.h);
    SDL_RenderCopy(renderer, text_texture, nullptr, &text_area);
    SDL_DestroyTexture(text_texture);
  }

  void text(SDL_Texture *texture, u32 x, u32 y) {
    SDL_Rect text_area = { x, y };
    SDL_QueryTexture(texture, nullptr, nullptr, &text_area.w, &text_area.h);
    SDL_RenderCopy(renderer, texture, nullptr, &text_area);
  }

  void line(u32 x0, u32 y0, u32 x1, u32 y1) {
    SDL_RenderDrawLine(renderer, x0, y0, x1, y1);
  }

  void polygon(SDL_Point const *points, u32 const size, b32 connected = true, b32 filled = false) {
    if (filled) {
      Sint16 *vx = new Sint16 [size];
      Sint16 *vy = new Sint16 [size];
      for (u32 i = 0; i < size; ++i) {
        vx[i] = points[i].x;
        vy[i] = points[i].y;
      }
      u32 c = getColor();
      u32 r = (c >> 24) & 0xff;
      u32 g = (c >> 16) & 0xff;
      u32 b = (c >>  8) & 0xff;
      u32 a = (c >>  0) & 0xff;
      filledPolygonRGBA(renderer, vx, vy, size, r, g, b, a);
      delete [] vx;
      delete [] vy;
    } else {
      for (u32 i = 1; i < size; ++i)
        SDL_RenderDrawLine(renderer, points[i-1].x, points[i-1].y, points[i].x, points[i].y);
      if (connected)
        SDL_RenderDrawLine(renderer, points[size-1].x, points[size-1].y, points[0].x, points[0].y);
    }
  }
}

/* End Renderer Stuff */

/* New UI Stuff */

#ifdef IMGUI_SRC_ID
#define GEN_ID ((IMGUI_SRC_ID) + (__LINE__))
#else
#define GEN_ID (__LINE__)
#endif

namespace gui {
  struct Context {
    u32 hot;
    u32 active;
  };
  Context context;

  void vertPanel(s32 id, String const &title, u32 x, u32 y, u32 w, u32 h, r32 h_scroll = -1, r32 v_scroll = -1) {
    u32 s = 8;

    SDL_Texture *title_texture = render::createText(title);
    SDL_Point text_size = render::textSize(title_texture);
    render::text(title_texture, x + s*5, y);
    SDL_DestroyTexture(title_texture);

    u32 tw = text_size.x;

    // top
    SDL_Point points [] = {
      { x + tw + s*6, y + s },
      { x + w,        y + s },
      { x + w,        y + s*2 },
      { x + s*2,      y + s*2 },
      { x,            y + s*6 },
      { x,            y + s*5 },
      { x + s*2,      y + s },
      { x + s*4,      y + s },
    };
    render::polygon(points, 8, false);

    // corner
    render::line(x + w - s*2, y + h, x + w, y + h - s*4);

    if (v_scroll < 0 || v_scroll > 1) {
      // left
      render::line(x, y + s*6, x, y + h);

      // right
      render::line(x + w, y + s*2, x + w, y + h - s*4);
    } else {
      // left
      u32 left_min = y + s*7;
      u32 left_max = y + h - s;
      u32 left_pos = left_min + (v_scroll * (left_max - left_min));
      render::line(x, left_pos - s, x, left_pos + s);

      // right
      u32 right_min = y + s*3;
      u32 right_max = y + h - s*5;
      u32 right_pos = right_min + (v_scroll * (right_max - right_min));
      render::line(x + w, right_pos - s, x + w, right_pos + s);
    }

    // bottom
    if (h_scroll < 0 || h_scroll > 1) {
      render::line(x, y + h, x + w - s*2, y + h);
    } else {
      u32 min = x + s;
      u32 max = x + w - s*3;
      u32 pos = min + (h_scroll * (max - min));
      render::line(pos - s, y + h, pos + s, y + h);
    }
  }

  b32 textBox(s32 id, String const &text, u32 x, u32 y, s32 w = -1) {
    u32 s = 8;

    SDL_Texture *text_texture = render::createText(text);
    SDL_Point text_size = render::textSize(text_texture);

    if (w < 0 || w < (text_size.x + s*3))
      w = text_size.x + s*3;
    u32 h = text_size.y;
    u32 offset = s - (w % s);
    w += offset;

    render::text(text_texture, x + s*2, y);
    SDL_DestroyTexture(text_texture);

    if (tracker::inRect(x,y,w,h)) {
      context.hot = id;
      if (!context.active && tracker::held)
        context.active = id;
    }

    SDL_Point points [] = {
      { x + s,     y     },
      { x,         y + h },
      { x + w - s, y + h },
      { x + w,     y     },
    };
    render::polygon(points, 4, false);

    return (!tracker::held &&
            id == context.hot &&
            id == context.active);
  }

  void begin() {
    context.hot = 0;
  }

  void end() {
    if (!tracker::held)
      context.active = 0;
    else if (0 == context.active)
      context.active = -1;
  }

  b32 label(s32 id, String const &text, u32 x, u32 y, s32 w = -1, s32 h = -1) {
    SDL_Texture *text_texture = render::createText(text);
    SDL_Point text_size = render::textSize(text_texture);

    if (w < text_size.x) w = text_size.x;
    if (h < text_size.y) h = text_size.y;
    s32 x_offset = (w - text_size.x) >> 1;
    s32 y_offset = (h - text_size.y) >> 1;

    render::text(text_texture, x + x_offset, y + y_offset);

    if (tracker::inRect(x,y,w,h)) {
      context.hot = id;
      if (0 == context.active && tracker::held)
        context.active = id;
    }

    SDL_DestroyTexture(text_texture);

    /* If button is hot and active,
     * but mouse button is not down,
     * the user must have clicked the button. */
    return (!tracker::held &&
            id == context.hot &&
            id == context.active);
  }

  b32 button(s32 id, String const &text, u32 x, u32 y, s32 w = -1, s32 h = -1) {
    u32 s = 8;

    SDL_Texture *text_texture = render::createText(text);
    SDL_Point text_size = render::textSize(text_texture);

    if (w < 0 || w < (text_size.x + s*2))
      w = text_size.x + s*2;
    if (h < 0 || h < text_size.y)
      h = text_size.y;
    u32 x_offset = s - (w % s);
    w += x_offset;

    render::text(text_texture, x + s + (x_offset >> 1), y);
    SDL_DestroyTexture(text_texture);

    SDL_Point points [] = {
      { x + s,     y     },
      { x,         y + h },
      { x + w - s, y + h },
      { x + w,     y     },
    };

    if (tracker::inRect(x,y,w,h)) {
      context.hot = id;
      if (0 == context.active && tracker::held)
        context.active = id;
    }

    /* Render */
    if (context.hot == id) {
      if (context.active == id) {
        // hovering and active
        render::polygon(points, 4, true, true);
      } else {
        // hovering
        render::polygon(points, 4, true, false);
      }
    } else {
      if (context.active == id) {
        // active
        render::polygon(points, 4, true, false);
      } else {
        // none
        render::polygon(points, 4, true, false);
      }
    }


    /* If button is hot and active,
     * but mouse button is not down,
     * the user must have clicked the button. */
    return (!tracker::held &&
            id == context.hot &&
            id == context.active);
  }

  b32 selector(s32 id, String const &text, u32 x, u32 y) {
    u32 s = 8;

    SDL_Texture *text_texture = render::createText(text);
    SDL_Point text_size = render::textSize(text_texture);

    u32 w = text_size.x + s*4;
    u32 h = text_size.y;
    u32 offset = s - (w % s);
    w += offset;

    render::text(text_texture, x + s*2 + (offset >> 1), y);
    SDL_DestroyTexture(text_texture);

    if (tracker::inRect(x,y,w,h)) {
      context.hot = id;
      if (0 == context.active && tracker::held)
        context.active = id;
    }

    SDL_Point inner [] = {
      { x + s*2,     y     },
      { x + s,       y + h },
      { x + w - s*2, y + h },
      { x + w - s,   y     },
    };

    SDL_Point outer [] = {
      { x + s,     y     },
      { x,         y + h },
      { x + w - s, y + h },
      { x + w,     y     },
    };

    if (context.hot == id) {
      if (context.active == id) {
        // hovering and active
        render::polygon(outer, 4, true, true);
      } else {
        // hovering
        SDL_Point left [] = {
          { x + s,   y     },
          { x,       y + h },
          { x + s,   y + h },
          { x + s*2, y     },
        };

        SDL_Point right [] = {
          { x + w,       y     },
          { x + w - s,   y + h },
          { x + w - s*2, y + h },
          { x + w - s,   y     },
        };

        render::polygon(inner, 4, true, false);
        render::polygon(left,  4, true, true);
        render::polygon(right, 4, true, true);
      }
    } else {
      if (context.active == id) {
        // active
        render::polygon(outer, 4, true, true);
      } else {
        // none
        render::polygon(inner, 4, false, false);
        render::polygon(outer, 4, true, false);
      }
    }

    return (!tracker::held &&
            id == context.hot &&
            id == context.active);
  }

}

/* End UI Stuff */

internal u32 app_red   = 0xff0000ff;
internal u32 app_green = 0x00ff88ff;
internal u32 app_blue  = 0x0088ffff;

void screen_logo() {
  //TODO(seth): display animated(?) logo screen
}

void screen_help() {
  //TODO(seth): display shortcut-key screen
  render::text("Escape", 8, 32);
  gui::textBox(GEN_ID, "Close MekTool", 64, 32);

  render::text("Tab", 8, 56);
  gui::textBox(GEN_ID, "Toggle Help screen", 64, 56);
}

u32 selected_servo = 0;
u32 active_selector = 0;

inline
void toggleActiveSelector(u32 selector_id) {
  active_selector = (active_selector == selector_id) ? 0 : selector_id;
}

void setConfiguration(s32 gui_id, String const &mekton, Form form, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(form), x, 0, w, 24)) {
    mekton::mektonConfiguration(mekton, form);
    active_selector = 0;
  }
}

void setPowerplant(s32 gui_id, String const &mekton, Powerplant powerplant, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(powerplant), x, 0, w, 24)) {
    mekton::mektonPowerplant(mekton, powerplant);
    active_selector = 0;
  }
}

void setServoType(s32 gui_id, u32 servo_id, ServoType servo_type, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(servo_type), x, 0, w, 24)) {
    mekton::servoType(servo_id, servo_type);
    active_selector = 0;
  }
}

void setServoArmor(s32 gui_id, u32 servo_id, Grade grade, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(grade), x, 0, w, 24)) {
    mekton::servoArmor(servo_id, grade);
    active_selector = 0;
  }
}

void setServoGrade(s32 gui_id, u32 servo_id, Grade grade, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(grade), x, 0, w, 24)) {
    mekton::servoGrade(servo_id, grade);
    active_selector = 0;
  }
}

void setEquipmentType(s32 gui_id, u32 equipment_id, EquipmentType type, Equipment equipment, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(type), x, 0, w, 24)) {
    mekton::equipment(equipment_id, equipment);
    active_selector = 0;
  }
}

void setEquipment(s32 gui_id, u32 equipment_id, Equipment equipment, u32 x, u32 w) {
  if (gui::label(gui_id, mekton::toString(equipment), x, 0, w, 24)) {
    mekton::equipment(equipment_id, equipment);
    active_selector = 0;
  }
}

void showBeamOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::LightBeamGun,      0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::MediumBeamGun,    80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::HeavyBeamGun,    160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::BeamCannon,      240, 80); render::line(320, 0, 320, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::HeavyBeamCannon, 320, 80); render::line(400, 0, 400, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::NovaCannon,      400, 80); render::line(480, 0, 480, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::PulseCannon,     480, 80); render::line(560, 0, 560, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::BeamSweeper,     560, 80); render::line(640, 0, 640, 24);
}

void showProjectileOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::LightCannon,     0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::MediumCannon,    80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::HeavyCannon,     160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::GiantCannon,     240, 80); render::line(320, 0, 320, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Autocannon,      320, 80); render::line(400, 0, 400, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::HeavyAutocannon, 400, 80); render::line(480, 0, 480, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::EpoxyGun,        480, 80); render::line(560, 0, 560, 24);
}

void showMissileOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::RocketPod,        0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::RocketLauncher,  80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::MissilePod,     160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::HeavyMissile,   240, 80); render::line(320, 0, 320, 24);
}

void showMeleeOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::Sword,       0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Axe,        80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Mace,      160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Drill,     240, 80); render::line(320, 0, 320, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Saw,       320, 80); render::line(400, 0, 400, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::ShockWhip, 400, 80); render::line(480, 0, 480, 24);
}

void showEnergyMeleeOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::EnergySword,   0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::EnergyAxe,    80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::EnergyLance, 160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::NovaSword,   240, 80); render::line(320, 0, 320, 24);
}

void showArmExtremityOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::Hand,       0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::ArmClaw,   80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::ArmTalon, 160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Pincer,   240, 80); render::line(320, 0, 320, 24);
}

void showLegExtremityOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::Foot,       0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::LegClaw,   80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::LegTalon, 160, 80); render::line(240, 0, 240, 24);
}

void showCockpitOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::MainCockpit,      0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::PassengerSpace,  80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::EjectionSeat,   160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::EnvironmentPod, 240, 80); render::line(320, 0, 320, 24);
}

void showSensorOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::MainSensor,    0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::BackupSensor, 80, 80); render::line(160, 0, 160, 24);
}

void showShieldOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::Small,    0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Medium,  80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Large,  160, 80); render::line(240, 0, 240, 24);
}

void showAssemblyOptions(u32 equipment_id) {
  setEquipment(GEN_ID, equipment_id, Equipment::AntiTheftCodeLock,      0, 80); render::line( 80, 0,  80, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::AntiTheftCodeAlarm,    80, 80); render::line(160, 0, 160, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::DamageControlPackage, 160, 80); render::line(240, 0, 240, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Spotlights,           240, 80); render::line(320, 0, 320, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Liftwire,             320, 80); render::line(400, 0, 400, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::Micromanipulators,    400, 80); render::line(480, 0, 480, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::StereoSystem,         480, 80); render::line(560, 0, 560, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::StorageModule,        560, 80); render::line(640, 0, 640, 24);
  setEquipment(GEN_ID, equipment_id, Equipment::WeaponLink,           640, 80); render::line(720, 0, 720, 24);
}

void screen_main(String const &mekton, s32 *servo_offset,
                 u32 *selected_equipment_id, s32 *equipment_offset) {

  u32 scroll_speed = 8;

  /* Mekton Info Panel */
  gui::vertPanel(GEN_ID, "Mekton Stats", 8, 32, 304, 232);

  gui::textBox(GEN_ID, mekton, 24, 72, 112);
  render::text("Weight", 152, 72);
  gui::textBox(GEN_ID, String(mekton::mektonWeight(mekton), 1), 200, 72);

  render::text("MV", 24, 96);
  gui::textBox(GEN_ID, String(mekton::mektonMV(mekton), 0), 48, 96);
  render::text("Cost", 152, 96);
  gui::textBox(GEN_ID, String(mekton::mektonCost(mekton), 1), 200, 96);

  render::text("Configuration", 24, 120);
  u32 const mekton_configuration_id = GEN_ID;
  if (gui::selector(mekton_configuration_id, mekton::mektonConfiguration(mekton), 112, 120))
    toggleActiveSelector(mekton_configuration_id);

  if (active_selector == mekton_configuration_id) {
    render::setColor(render::White);
    setConfiguration(GEN_ID, mekton, Form::Mekton,         0, 64); render::line( 64, 0,  64, 24);
    setConfiguration(GEN_ID, mekton, Form::Mechabeast,    64, 96); render::line(160, 0, 160, 24);
    setConfiguration(GEN_ID, mekton, Form::Mechatank,    160, 96); render::line(256, 0, 256, 24);
    setConfiguration(GEN_ID, mekton, Form::Mechafighter, 256, 96); render::line(352, 0, 352, 24);
    render::setColor(app_blue);
  }

  render::text("Powerplant", 24, 144);
  u32 const mekton_powerplant_id = GEN_ID;
  if (gui::selector(mekton_powerplant_id, mekton::mektonPowerplant(mekton), 112, 144))
    toggleActiveSelector(mekton_powerplant_id);

  if (active_selector == mekton_powerplant_id) {
    render::setColor(render::White);
    setPowerplant(GEN_ID, mekton, Powerplant::Cool,  0, 48); render::line(48, 0, 48, 24);
    setPowerplant(GEN_ID, mekton, Powerplant::Hot,  48, 48); render::line(96, 0, 96, 24);
    render::setColor(app_blue);
  }

  s32 bonus_ma = mekton::mektonBonusMA(mekton);
  render::text("Land MA", 24, 168);
  if (gui::button(GEN_ID, "<", 112, 168) && bonus_ma >  0) mekton::mektonBonusMA(mekton, --bonus_ma);
  gui::textBox(GEN_ID, mekton::mektonLandMA(mekton) + bonus_ma, 128, 168, 32);
  if (gui::button(GEN_ID, ">", 160, 168) && bonus_ma < 99) mekton::mektonBonusMA(mekton, ++bonus_ma);

  s32 flight_ma = mekton::mektonFlightMA(mekton);
  render::text("Flight MA", 24, 192);
  if (gui::button(GEN_ID, "<", 112, 192) && flight_ma >  0) mekton::mektonFlightMA(mekton, --flight_ma);
  gui::textBox(GEN_ID, flight_ma, 128, 192, 32);
  if (gui::button(GEN_ID, ">", 160, 192) && flight_ma < 99) mekton::mektonFlightMA(mekton, ++flight_ma);

  s32 num_thrusters = mekton::mektonThrusters(mekton);
  render::text("# of Thrusters", 24, 216);
  if (gui::button(GEN_ID, "<", 112, 216) && num_thrusters >  0) mekton::mektonThrusters(mekton, --num_thrusters);
  gui::textBox(GEN_ID, num_thrusters, 128, 216, 32);
  if (gui::button(GEN_ID, ">", 160, 216) && num_thrusters < 99) mekton::mektonThrusters(mekton, ++num_thrusters);

  s32 num_ges = mekton::mektonGES(mekton);
  render::text("# of GES", 24, 240);
  if (gui::button(GEN_ID, "<", 112, 240) && num_ges >  0) mekton::mektonGES(mekton, --num_ges);
  gui::textBox(GEN_ID, num_ges, 128, 240, 32);
  if (gui::button(GEN_ID, ">", 160, 240) && num_ges < 99) mekton::mektonGES(mekton, ++num_ges);

  /* Servo Info Panel */
  {
    render::text("Weight", 416, 48);
    gui::textBox(GEN_ID, String(mekton::mektonServoWeight(mekton), 1), 464, 48, 48);
    render::text("Cost", 528, 48);
    gui::textBox(GEN_ID, String(mekton::mektonServoCost(mekton), 1), 560, 48, 48);

    render::setClipRect(344, 64, 272, 568);
    List<s32> *servo_list = mekton::mektonServoList(mekton);
    u32 x = 344;
    u32 y = 64 + *servo_offset;
    u32 h = 208;

    u32 servo_list_height = 0;
    ForEach (*servo_list) {
      if (tracker::isClicked(x, y, 271, h)) {
        selected_servo = *it;
        *selected_equipment_id = 0;
        *equipment_offset = 0;
      }

      render::setColor(app_blue);
      if (*it == selected_servo)
        render::setColor(app_green);

      gui::vertPanel(GEN_ID, "Servo", x, y, 271, h);

      //TODO(seth): add a remove servo button!!

      render::text("Type", x+16, y+40);
      u32 const servo_type_id = GEN_ID + it.index;
      if (gui::selector(servo_type_id, mekton::servoType(*it), x+64, y+40))
        toggleActiveSelector(servo_type_id);

      if (active_selector == servo_type_id) {
        render::clearClipRect();
        render::setColor(render::White);
        setServoType(GEN_ID, *it, ServoType::Torso,   0, 64); render::line( 64, 0,  64, 24);
        setServoType(GEN_ID, *it, ServoType::Arm,    64, 64); render::line(128, 0, 128, 24);
        setServoType(GEN_ID, *it, ServoType::Leg,   128, 64); render::line(192, 0, 192, 24);
        setServoType(GEN_ID, *it, ServoType::Head,  192, 64); render::line(256, 0, 256, 24);
        setServoType(GEN_ID, *it, ServoType::Tail,  256, 64); render::line(320, 0, 320, 24);
        setServoType(GEN_ID, *it, ServoType::Wing,  320, 64); render::line(384, 0, 384, 24);
        setServoType(GEN_ID, *it, ServoType::Pod,   384, 64); render::line(448, 0, 448, 24);
        render::setColor(*it == selected_servo ? app_green : app_blue);
        render::setClipRect(344, 64, 272, 568);
      }

      render::text("Kills", x+136, y+40);
      gui::textBox(GEN_ID + it.index, mekton::servoKills(*it), x+168, y+40);

      render::text("Weight", x+16, y+64);
      gui::textBox(GEN_ID + it.index, mekton::servoWeight(*it), x+64, y+64);
      render::text("Cost", x+136, y+64);
      gui::textBox(GEN_ID + it.index, mekton::servoCost(*it), x+168, y+64);

      render::text("Armor", x+16, y+88);
      u32 const servo_armor_id = GEN_ID + it.index;
      if (gui::selector(servo_armor_id, mekton::servoArmor(*it), x+56, y+88))
        toggleActiveSelector(servo_armor_id);

      if (active_selector == servo_armor_id) {
        tracker::setBoundingRect(0, 0,  720, 24);
        render::setClipRect(0, 0, 720, 24);
        render::setColor(render::White);
        setServoArmor(GEN_ID, *it, Grade::SuperLight,      0,  80); render::line( 80, 0,  80, 24);
        setServoArmor(GEN_ID, *it, Grade::LightWeight,    80,  96); render::line(176, 0, 176, 24);
        setServoArmor(GEN_ID, *it, Grade::Striker,       176,  56); render::line(232, 0, 232, 24);
        setServoArmor(GEN_ID, *it, Grade::MediumStriker, 232, 104); render::line(336, 0, 336, 24);
        setServoArmor(GEN_ID, *it, Grade::HeavyStriker,  336,  96); render::line(432, 0, 432, 24);
        setServoArmor(GEN_ID, *it, Grade::MediumWeight,  432, 104); render::line(536, 0, 536, 24);
        setServoArmor(GEN_ID, *it, Grade::LightHeavy,    536,  80); render::line(616, 0, 616, 24);
        setServoArmor(GEN_ID, *it, Grade::MediumHeavy,   616,  96); render::line(712, 0, 712, 24);
        setServoArmor(GEN_ID, *it, Grade::ArmoredHeavy,  712, 104); render::line(816, 0, 816, 24);
        setServoArmor(GEN_ID, *it, Grade::SuperHeavy,    816,  96); render::line(912, 0, 912, 24);
        setServoArmor(GEN_ID, *it, Grade::MegaHeavy,     912,  80); render::line(992, 0, 992, 24);
        render::setColor(*it == selected_servo ? app_green : app_blue);
        render::setClipRect(344, 64, 272, 568);
        tracker::clearBoundingRect();
      }

      render::text("SP", x+186, y+88);
      gui::textBox(GEN_ID + it.index, mekton::servoArmorSP(*it), x+208, y+88);

      render::text("Grade", x+16, y+112);
      u32 const servo_grade_id = GEN_ID + it.index;
      if (gui::selector(servo_grade_id, mekton::servoGrade(*it), x+56, y+112))
        toggleActiveSelector(servo_grade_id);

      if (active_selector == servo_grade_id) {
        tracker::setBoundingRect(0, 0, 720, 24);
        render::setClipRect(0, 0, 720, 24);
        render::setColor(render::White);
        setServoGrade(GEN_ID, *it, Grade::SuperLight,      0,  80); render::line( 80, 0,  80, 24);
        setServoGrade(GEN_ID, *it, Grade::LightWeight,    80,  96); render::line(176, 0, 176, 24);
        setServoGrade(GEN_ID, *it, Grade::Striker,       176,  56); render::line(232, 0, 232, 24);
        setServoGrade(GEN_ID, *it, Grade::MediumStriker, 232, 104); render::line(336, 0, 336, 24);
        setServoGrade(GEN_ID, *it, Grade::HeavyStriker,  336,  96); render::line(432, 0, 432, 24);
        setServoGrade(GEN_ID, *it, Grade::MediumWeight,  432,  96); render::line(536, 0, 536, 24);
        setServoGrade(GEN_ID, *it, Grade::LightHeavy,    536,  80); render::line(616, 0, 616, 24);
        setServoGrade(GEN_ID, *it, Grade::MediumHeavy,   616,  96); render::line(712, 0, 712, 24);
        setServoGrade(GEN_ID, *it, Grade::ArmoredHeavy,  712, 104); render::line(816, 0, 816, 24);
        setServoGrade(GEN_ID, *it, Grade::SuperHeavy,    816,  96); render::line(912, 0, 912, 24);
        setServoGrade(GEN_ID, *it, Grade::MegaHeavy,     912,  80); render::line(992, 0, 992, 24);
        render::setColor(*it == selected_servo ? app_green : app_blue);
        render::setClipRect(344, 64, 272, 568);
        tracker::clearBoundingRect();
      }

      u32 num_thruster_spaces = mekton::servoThrusters(*it);
      render::text("Thruster Spaces", x+32, y+160);
      if (gui::button(GEN_ID + it.index, "<", x+128, y+160) && num_thruster_spaces >  0) mekton::servoThrusters(*it, --num_thruster_spaces);
      gui::textBox(GEN_ID, num_thruster_spaces, x+144, y+160, 32);
      if (gui::button(GEN_ID + it.index, ">", x+176, y+160) && num_thruster_spaces < 99) mekton::servoThrusters(*it, ++num_thruster_spaces);

      u32 num_ges_spaces = mekton::servoGES(*it);
      render::text("GES Spaces", x+32, y+184);
      if (gui::button(GEN_ID + it.index, "<", x+128, y+184) && num_ges_spaces >  0) mekton::servoGES(*it, --num_ges_spaces);
      gui::textBox(GEN_ID, num_ges_spaces, x+144, y+184, 32);
      if (gui::button(GEN_ID + it.index, ">", x+176, y+184) && num_ges_spaces < 99) mekton::servoGES(*it, ++num_ges_spaces);

      render::text("Space", x+16, y+136);
      r32 servo_space_max  = mekton::servoSpace(*it);
      r32 servo_space_used = mekton::servoEquipmentSpace(*it) + num_thruster_spaces + num_ges_spaces;
      String servo_space;
      servo_space << (servo_space_max - servo_space_used) << " / " << servo_space_max;
      gui::textBox(GEN_ID + it.index, servo_space, x+64, y+136);

      y += h + 8;
      servo_list_height += h + 8;
    }
    render::clearClipRect();
    render::setColor(app_blue);

    u32 panel_x = 328;
    u32 panel_y = 32;
    u32 panel_w = 304;
    u32 panel_h = 600;

    s32 min_offset = (panel_h - panel_y) - servo_list_height;
    gui::vertPanel(GEN_ID, "Servo Stats", panel_x, panel_y, panel_w, panel_h, -1, (r32)(*servo_offset) / (r32)min_offset);

    if (tracker::inRect(panel_x, panel_y, panel_w, panel_h)) {
      *servo_offset += tracker::wheel * scroll_speed;

      if (*servo_offset < min_offset) *servo_offset = min_offset;
      if (*servo_offset >          0) *servo_offset = 0;
    }

    delete servo_list;
  }

  /* Equipment Info Panel */
  {
    render::text("Weight", 736, 48);
    gui::textBox(GEN_ID, String(mekton::servoEquipmentWeight(selected_servo), 1), 784, 48, 48);
    render::text("Cost", 848, 48);
    gui::textBox(GEN_ID, String(mekton::servoEquipmentCost(selected_servo), 1), 880, 48, 48);

    render::setClipRect(664, 64, 272, 568);
    List<s32> *equipment_list = mekton::servoEquipmentList(selected_servo);
    u32 x = 664;
    u32 y = 64 + *equipment_offset;

    u32 equipment_list_height = 0;
    ForEach (*equipment_list) {
      u32 equipment_type = mekton::equipmentTypeId(*it);

      u32 h;
      switch (equipment_type) {
        case EquipmentType::Beam:         h = 208; break;
        case EquipmentType::Projectile:   h = 208; break;
        case EquipmentType::Missile:      h = 184; break;
        case EquipmentType::Melee:        h = 208; break;
        case EquipmentType::EnergyMelee:  h = 184; break;
        case EquipmentType::ArmExtremity: h = 160; break;
        case EquipmentType::LegExtremity: h = 160; break;
        case EquipmentType::Cockpit:      h = 136; break;
        case EquipmentType::Sensor:       h = 160; break;
        case EquipmentType::Shield:       h = 136; break;
        case EquipmentType::Assembly:     h = 136; break;
        default: h = 0; break;
      }

      if (tracker::isClicked(x, y, 271, h))
        *selected_equipment_id = *it;

      render::setColor(app_blue);
      if (*it == *selected_equipment_id)
        render::setColor(app_green);

      gui::vertPanel(GEN_ID + it.index, "Equipment", x, y, 271, h);

      //TODO(seth): add a remove equipment button!!

      render::text("Name", x+16, y+40);
      u32 const equipment_name_id = GEN_ID + it.index;
      if (gui::selector(GEN_ID + it.index, mekton::equipmentName(*it), x+64, y+40))
        toggleActiveSelector(equipment_name_id);

      if (active_selector == equipment_name_id) {
        tracker::setBoundingRect(0, 0, 720, 24);
        render::setClipRect(0, 0, 720, 24);
        render::setColor(render::White);
        switch (equipment_type) {
          case EquipmentType::Beam:         showBeamOptions(*it);         break;
          case EquipmentType::Projectile:   showProjectileOptions(*it);   break;
          case EquipmentType::Missile:      showMissileOptions(*it);      break;
          case EquipmentType::Melee:        showMeleeOptions(*it);        break;
          case EquipmentType::EnergyMelee:  showEnergyMeleeOptions(*it);  break;
          case EquipmentType::ArmExtremity: showArmExtremityOptions(*it); break;
          case EquipmentType::LegExtremity: showLegExtremityOptions(*it); break;
          case EquipmentType::Cockpit:      showCockpitOptions(*it);      break;
          case EquipmentType::Sensor:       showSensorOptions(*it);       break;
          case EquipmentType::Shield:       showShieldOptions(*it);       break;
          case EquipmentType::Assembly:     showAssemblyOptions(*it);     break;
          default: break;
        }
        render::setColor(*it == *selected_equipment_id ? app_green : app_blue);
        render::setClipRect(664, 64, 272, 568);
        tracker::clearBoundingRect();
      }

      render::text("Type", x+16, y+64);
      u32 const equipment_type_id = GEN_ID + it.index;
      if (gui::selector(GEN_ID + it.index, mekton::toString((EquipmentType)equipment_type), x+64, y+64))
        toggleActiveSelector(equipment_type_id);

      if (active_selector == equipment_type_id) {
        tracker::setBoundingRect(0, 0, 720, 24);
        render::setClipRect(0, 0, 720, 24);
        render::setColor(render::White);
        setEquipmentType(GEN_ID, *it, EquipmentType::Beam,         Equipment::LightBeamGun,        0, 80); render::line( 80, 0,  80, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Projectile,   Equipment::LightCannon,        80, 80); render::line(160, 0, 160, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Missile,      Equipment::RocketPod,         160, 80); render::line(240, 0, 240, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Melee,        Equipment::Sword,             240, 80); render::line(320, 0, 320, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::EnergyMelee,  Equipment::EnergySword,       320, 80); render::line(400, 0, 400, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::ArmExtremity, Equipment::Hand,              400, 80); render::line(480, 0, 480, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::LegExtremity, Equipment::Foot,              480, 80); render::line(560, 0, 560, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Cockpit,      Equipment::MainCockpit,       560, 80); render::line(640, 0, 640, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Sensor,       Equipment::MainSensor,        640, 80); render::line(720, 0, 720, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Shield,       Equipment::Small,             720, 80); render::line(800, 0, 800, 24);
        setEquipmentType(GEN_ID, *it, EquipmentType::Assembly,     Equipment::AntiTheftCodeLock, 800, 80); render::line(880, 0, 880, 24);
        render::setColor(*it == *selected_equipment_id ? app_green : app_blue);
        render::setClipRect(664, 64, 272, 568);
        tracker::clearBoundingRect();
      }

      render::text("Cost", x+16, y+88);
      gui::textBox(GEN_ID + it.index, mekton::equipmentCost(*it), x+64, y+88);

      switch (equipment_type) {
        case EquipmentType::Beam: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+176, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("Range", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentRange(*it), x+176, y+136);

          render::text("WA", x+16, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentWA(*it), x+64, y+160);
          render::text("Shots", x+136, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentShots(*it), x+176, y+160);

          render::text("BV", x+16, y+184);
          gui::textBox(GEN_ID + it.index, mekton::equipmentBV(*it), x+64, y+184);
        } break;

        case EquipmentType::Projectile: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+176, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("Range", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentRange(*it), x+176, y+136);

          render::text("WA", x+16, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentWA(*it), x+64, y+160);
          render::text("Shots", x+136, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentShots(*it), x+176, y+160);

          render::text("BV", x+16, y+184);
          gui::textBox(GEN_ID + it.index, mekton::equipmentBV(*it), x+64, y+184);
        } break;

        case EquipmentType::Missile: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+176, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("Range", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentRange(*it), x+176, y+136);

          render::text("WA", x+16, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentWA(*it), x+64, y+160);
          render::text("Shots", x+136, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentShots(*it), x+176, y+160);
        } break;

        case EquipmentType::Melee: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+176, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("Range", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentRange(*it), x+176, y+136);

          render::text("WA", x+16, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentWA(*it), x+64, y+160);
          render::text("Shots", x+136, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentShots(*it), x+176, y+160);

          render::text("AP", x+16, y+184);
          gui::textBox(GEN_ID + it.index, mekton::equipmentAP(*it), x+64, y+184);
        } break;

        case EquipmentType::EnergyMelee: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+176, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("Range", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentRange(*it), x+176, y+136);

          render::text("WA", x+16, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentWA(*it), x+64, y+160);
          render::text("Shots", x+136, y+160);
          gui::textBox(GEN_ID + it.index, mekton::equipmentShots(*it), x+176, y+160);
        } break;

        case EquipmentType::ArmExtremity: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+216, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("Manipulation", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentManipulation(*it), x+216, y+136);
        } break;

        case EquipmentType::LegExtremity: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+200, y+112);

          render::text("Damage", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDamage(*it), x+64, y+136);
          render::text("MA Penalty", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentMAPenalty(*it), x+200, y+136);
        } break;

        case EquipmentType::Cockpit: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Crew", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentCrew(*it), x+176, y+112);
        } break;

        case EquipmentType::Sensor: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
          render::text("Kills", x+136, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentKills(*it), x+176, y+112);

          render::text("Range", x+16, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentRange(*it), x+64, y+136);
          render::text("Comm", x+136, y+136);
          gui::textBox(GEN_ID + it.index, mekton::equipmentComm(*it), x+176, y+136);
        } break;

        case EquipmentType::Shield: {
          render::text("DA", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentDA(*it), x+48, y+112);
          render::text("SP", x+80, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSP(*it), x+112, y+112);
        } break;

        case EquipmentType::Assembly: {
          render::text("Space", x+16, y+112);
          gui::textBox(GEN_ID + it.index, mekton::equipmentSpace(*it), x+64, y+112);
        } break;
      }

      y += h + 8;
      equipment_list_height += h + 8;
    }
    render::clearClipRect();
    render::setColor(app_blue);

    u32 panel_x = 648;
    u32 panel_y = 32;
    u32 panel_w = 304;
    u32 panel_h = 600;

    s32 min_offset = (panel_h - panel_y) - equipment_list_height;
    r32 equipment_scroll_bar = (r32)(*equipment_offset) / (r32)min_offset;
    if (0 < min_offset)
      equipment_scroll_bar = -1;
    gui::vertPanel(GEN_ID, "Equipment Stats", panel_x, panel_y, panel_w, panel_h, -1, equipment_scroll_bar);

    if (tracker::inRect(panel_x, panel_y, panel_w, panel_h)) {
      *equipment_offset += tracker::wheel * scroll_speed;

      if (*equipment_offset < min_offset) *equipment_offset = min_offset;
      if (*equipment_offset >          0) *equipment_offset = 0;
    }

    delete equipment_list;
  }
}

enum struct Screen {
  Logo,
  Main,
  Help,
};

int main() {
  /* Test Area */

  Database db;
  db.open("mektool.db", "US");
  mekton::setupDatabase(db);

  String const &rapier = testRapier();
  // String const &gorgon = testGorgon(db);

  /* End Test Area */

  if (SDL_Init(0) < 0)
    render::showError("Main App", "", SDL_GetError());

  b32 running = true;
  //TODO(seth): This should start as Screen::Logo
  Screen screen_mode = Screen::Main;
  s32 servo_offset      = 0;
  u32 selected_equipment_id = 0;
  s32 equipment_offset      = 0;

  render::init(960, 640);
  render::setTitle("Mekton Test");
  render::captureInput(true);

  /* Main Loop */
  SDL_StartTextInput();

  while (running) {
    timer::update();

    render::clear();
    render::setColor(render::White);

    /* Process Events */
    if (tracker::held)
      tracker::triggered = false;

    tracker::wheel = 0;
    SDL_Event event = {};
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_MOUSEBUTTONDOWN: {
          tracker::held = true;
          tracker::triggered = true;
        } break;

        case SDL_MOUSEMOTION: {
          tracker::x = event.motion.x;
          tracker::y = event.motion.y;
        } break;

        case SDL_MOUSEWHEEL: {
          tracker::wheel += event.wheel.y;
          // servo_offset += event.wheel.y * 8;
          // equipment_offset += event.wheel.y * 8;
        } break;

        case SDL_MOUSEBUTTONUP: {
          tracker::held = false;
        } break;

        case SDL_TEXTINPUT: {
        } break;

        case SDL_KEYDOWN: {
          switch (event.key.keysym.scancode) {
            case SDL_SCANCODE_BACKSPACE: {
            } break;

            case SDL_SCANCODE_TAB: {
              switch (screen_mode) {
                case Screen::Logo: break;
                case Screen::Main: screen_mode = Screen::Help; break;
                case Screen::Help: screen_mode = Screen::Main; break;
              }
            } break;

            case SDL_SCANCODE_ESCAPE: {
              running = false;
            } break;

            case SDL_SCANCODE_UP: {
            } break;

            case SDL_SCANCODE_DOWN: {
            } break;

            case SDL_SCANCODE_RIGHT: {
            } break;

            case SDL_SCANCODE_LEFT: {
            } break;

            default: {
              // do nothing
            } break;
          }
        } break;
      }
    }

    gui::begin();

    render::setColor(app_blue);
    if (gui::button(GEN_ID, "Help", 720, 0))
      switch (screen_mode) {
        case Screen::Logo: break;
        case Screen::Main: screen_mode = Screen::Help; break;
        case Screen::Help: screen_mode = Screen::Main; break;
      }

    if (gui::button(GEN_ID, "Clear", 768, 0)) {
      //TODO(seth): clear all current mekton data
    }

    if (gui::button(GEN_ID, "Load", 816, 0)) {
      //TODO(seth): load a Mekton
    }

    if (gui::button(GEN_ID, "Export", 864, 0)) {
      //TODO(seth): export the current Mekton to a text/pdf/something file
    }

    if (gui::button(GEN_ID, "Exit", 920, 0, 32))
      running = false;

    render::line(0, 24, 960, 24);

    switch (screen_mode) {
      case Screen::Logo:
        screen_logo();
        break;
      case Screen::Main:
        screen_main(rapier, &servo_offset, &selected_equipment_id, &equipment_offset);
        break;
      case Screen::Help:
        screen_help();
        break;
    }

    gui::end();

    render::present();
  }

  SDL_StopTextInput();

  render::close();

  SDL_Quit();

  return 0;
}