#pragma once

#include <stdio.h>
#include "sqlite3.h"
#include "list.hpp"
#include "string.hpp"

#include "types.h"

#if DEBUG
#define LOG(str) printf("--| SQL |--\n%s\n", str.text)
#define RESULT(str) printf("--| RESULT |--\n%s\n", #str)
#else
#define LOG(str)
#define RESULT(str)
#endif

struct Database {
  sqlite3 *db;
  String lang;

  void printError(String const& type, u32 code) {
    printf("sqlite %s error(%d): %s\n", type.text, code, sqlite3_errmsg(db));
  }

  b32 open(String const& database, String const& language = "US") {
    u32 code = sqlite3_open_v2(database.text, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nullptr);
    if (code != SQLITE_OK) {
      printError("db", code);
      return false;
    }
    lang = language;
    return true;
  }

  void close() {
    sqlite3_close(db);
  }

  void setLanguage(String const& language) {
    lang = language;
  }

  b32 run(String const& stmt) {
    return runStatement(stmt);
  }

  sqlite3_stmt *prepareStatement(String const& query) {
    LOG(query);
    sqlite3_stmt *stmt = nullptr;
    u32 code = sqlite3_prepare_v2(db, query.text, -1, &stmt, nullptr);
    if (code != SQLITE_OK)
      printError("stmt", code);
    return stmt;
  }

  enum class ResultCode {
    Unavailable,
    InProgress,
    Done,
    Error,
  };

  ResultCode statementOkay(u32 result_code) {
    switch (result_code) {
      case SQLITE_BUSY:
        // Unable to acquire the database lock.
        // If the statement was a COMMIT or was outside an explicit transaction, then you can retry.
        // Otherwise, rollback the transaction before continuing.
        RESULT(SQLITE_BUSY);
        return ResultCode::Unavailable;
      case SQLITE_DONE:
        // Finished executing successfully. No data to return. Call sqlite3_reset() or
        // prepare a new statement before calling sqlite3_step() again.
        RESULT(SQLITE_DONE);
        return ResultCode::Done;
      case SQLITE_ROW:
        // In the process of retrieving result data. This is returned each time data is
        // ready to be retrieved. Call sqlite3_step() again to continue polling results;
        RESULT(SQLITE_ROW);
        return ResultCode::InProgress;
      case SQLITE_MISUSE:
        // This routine was called inappropriately. Maybe the prepared statement was already
        // finalized, or SQLITE_ERROR or SQLITE_DONE was already returned, or the same database connection
        // is being used by multiple threads?
        RESULT(SQLITE_MISUSE);
        return ResultCode::Error;
      default:
        // A run-time error has occurred such as a constraint violation. Call sqlite3_errmsg()
        // for more information.
        printError("step", result_code);
        return ResultCode::Error;
    }
  }

  String *processString(String const& query) {
    sqlite3_stmt *stmt = prepareStatement(query);
    String *str = nullptr;
    ResultCode code = statementOkay(sqlite3_step(stmt));
    if (ResultCode::InProgress == code) {
      unsigned char const *temp = sqlite3_column_text(stmt, 0);
      if (temp)
        str = new String((char const *)temp);
    }
    sqlite3_finalize(stmt);
    return str;
  }

  List<s32> *processIntColumn(String const& query) {
    sqlite3_stmt *stmt = prepareStatement(query);
    List<s32> *list = new List<s32>;
    ResultCode result = statementOkay(sqlite3_step(stmt));
    while (ResultCode::InProgress == result) {
      list->push_back(sqlite3_column_int(stmt, 0));
      result = statementOkay(sqlite3_step(stmt));
    }
    sqlite3_finalize(stmt);
    return list;
  }

  s32 processInt(String const& query) {
    sqlite3_stmt *stmt = prepareStatement(query);
    ResultCode code = statementOkay(sqlite3_step(stmt));
    s32 result = -1;
    if (ResultCode::InProgress == code)
      result = sqlite3_column_int(stmt, 0);
    sqlite3_finalize(stmt);
    return result;
  }

  r64 processReal(String const& query) {
    sqlite3_stmt *stmt = prepareStatement(query);
    ResultCode code = statementOkay(sqlite3_step(stmt));
    r64 result = NAN;
    if (ResultCode::InProgress == code)
      result = sqlite3_column_double(stmt, 0);
    sqlite3_finalize(stmt);
    return result;
  }

  b32 runStatement(String const& query) {
    sqlite3_stmt *stmt = prepareStatement(query);
    ResultCode code = statementOkay(sqlite3_step(stmt));
    sqlite3_finalize(stmt);
    return (ResultCode::Done == code || ResultCode::InProgress == code);
  }

  u32 lastId() {
    return sqlite3_last_insert_rowid(db);
  }

  String *getText(String const& text) {
    String query("SELECT ");
    query << lang;
    query << " FROM Strings WHERE US = '";
    query << text;
    query << "';";
    return processString(query);
  }
};

#undef LOG