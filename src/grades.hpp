/*
Author: Seth Renshaw
Date Created: 11/7/2016
*/

#pragma once

#include "types.h"
#include "text_table.h"

enum class Grade {
  NONE,
  SuperLight    =  1,
  LightWeight   =  2,
  Striker       =  3,
  MediumStriker =  4,
  HeavyStriker  =  5,
  MediumWeight  =  6,
  LightHeavy    =  7,
  MediumHeavy   =  8,
  ArmoredHeavy  =  9,
  SuperHeavy    = 10,
  MegaHeavy     = 11,
  COUNT
};
