#pragma once

#include "types.h"

namespace audio {
  r32
  dBToVolume(r32 dB) {
    return powf(10.0f, 0.05f * dB);
  }

  r32
  volumeTodB(r32 volume) {
    return 20.0f * log10f(volume);
  }

  struct AudioEngine {
    static void init();
    static void update();
    static void shutdown();

    void
    loadSound(char const* sound_name, b32 is_looping = false, b32 is_streaming = false);

    void
    unloadSound(char const* sound_name);

    u32
    playSound(char const* sound_name, SDL_Point* pos = {0,0}, r32 volume_db = 0);

    void
    stopChannel(u32 channel_id);

    void
    stopAllChannels();

    void
    setChannelPosition(u32 channel_id, SDL_Point* pos);

    void
    setChannelVolume(u32 channel_id, r32 volume_db);

    b32
    isPlaying(u32 channel_id);
  };
}