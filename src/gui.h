/*
File: 'MekTool_CPP/gui.h'
Author: Seth Renshaw
Date Created: 7/17/2016
*/

#pragma once

#include "SDL_helper.h"
#include "app.h"
#include "string.h"
#include "timer.h"
#include "types.h"
#include "text_table.h"
#include "renderer.h"
#include "tracker.h"

#define ARRAY_COUNT(array) (sizeof(array)/sizeof(*array))
typedef SDL_Color Color;

internal u32 const VP_DEFAULT_OFFSET = 2;
internal u32 const TILE_SIZE         = 8;

namespace gui {
  b32 update_ui = true;
  SDL_Color color = { 255, 255, 255, 255 };

  internal inline
  u32 PX(u32 x) { return x * TILE_SIZE; }

  struct Layout {
    u32 x, y, start_x, start_y;
    void beginRow() { x = start_x; }
    void endRow(u32 h, u32 offset = 1) { y += h + offset; }
  };

  struct AnimInfo {
    r32 start_time;
    r32 fade_time;
    r32 extend_time;
    r32 text_fade_time;

    AnimInfo(r32 st  = 0.0f,
             r32 ft  = 0.2f,
             r32 et  = 0.1f,
             r32 tft = 0.5f)
    : start_time(st)
    , fade_time(ft)
    , extend_time(et)
    , text_fade_time(tft)
    {}

    inline
    b32 isFading(r32 accumulator) {
      return accumulator - start_time < fade_time;
    }

    inline
    b32 isExtending(r32 accumulator) {
      return accumulator - (start_time + fade_time) < extend_time;
    }

    inline
    b32 isTextFading(r32 accumulator) {
      return accumulator - (start_time + fade_time + extend_time) < text_fade_time;
    }

    inline
    r32 getFadeValue(r32 accumulator) {
      return (accumulator - start_time) / fade_time;
    }

    inline
    r32 getExtendValue(r32 accumulator) {
      return (accumulator - (start_time + fade_time)) / extend_time;
    }

    inline
    r32 getTextFadeValue(r32 accumulator) {
      return (accumulator - (start_time + fade_time + extend_time)) / text_fade_time;
    }
  };

  internal
  void drawText(Renderer* renderer, u32 x, u32 y, u32 w, u32 h, SDL_Texture* texture) {
    SDL_SetTextureAlphaMod(texture, renderer->getAlpha());
    SDL_SetTextureColorMod(texture, color.r, color.g, color.b);
    SDL_Rect text_area = { x, y, w, h };
    SDL_RenderCopy(renderer->renderer, texture, nullptr, &text_area);
  }

  void setRenderColor(SDL_Color c) {
    color = c;
  }

  void setClipRect(Renderer* renderer, u32 x, u32 y, u32 w, u32 h) {
    SDL_Rect clip_rect = { PX(x), PX(y), PX(w), PX(h) };
    SDL_RenderSetClipRect(renderer->renderer, &clip_rect);
  }

  void clearClipRect(Renderer* renderer) {
    SDL_RenderSetClipRect(renderer->renderer, nullptr);
  }

  void drawOrientedText(Renderer* renderer, Text* text, b32 is_vert, s32 x, s32 y, s32 w, s32 h) {
    if (text->single) {
      drawText(renderer, x, y, w, h, text->main_texture);
    } else {
      if (is_vert) {
        s32 h_os = h >> 1;
        drawText(renderer, x, y,        w, h_os, text->main_texture);
        drawText(renderer, x, y + h_os, w, h_os, text->extra_texture);
      } else {
        r32 ratio = text->textRatio();
        drawText(renderer, x,                     y, w * ratio,     h, text->main_texture);
        drawText(renderer, x + (w*ratio) + PX(1), y, w * (1-ratio), h, text->extra_texture);
      }
    }
  }

  void drawTextEdit(Renderer* renderer, Text* text, Tracker* mouse,
                   r32 accumulator, AnimInfo* anim,
                   u32 x, u32 y, u32 w, u32 h,
                   String* user_text, b32 enabled,
                   void(* click_func)(String*) = nullptr) {
    b32 text_check = user_text && user_text->size > 0;

    if (text_check) {
      text = new Text(user_text->text);
      text->loadTexture(renderer);
    }

    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    SDL_Point text_size = text->textSize(false);
    s32 x_os = x + ((w - text_size.x) >> 1);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
      w = PX(1);
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
      w = utils::lerp(PX(1), w, extend_value);
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      drawOrientedText(renderer, text, false, x_os, y, text_size.x, text_size.y);
      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
      drawOrientedText(renderer, text, false, x_os, y, text_size.x, text_size.y);
    }

    if (text_check) {
      delete text;
    }

    SDL_Point points [] = {
      { x + PX(1),     y },
      { x,             y + h },
      { x + w - PX(1), y + h },
      { x + w,         y },
    };
    renderer->renderPoly(points, ARRAY_COUNT(points), false);

    if (enabled && mouse->isClicked(x,y,w,h) && click_func) {
      click_func(user_text);
    }
  }

  b32 drawButton(Renderer* renderer, Text text_table [], Tracker* mouse,
                 r32 accumulator, AnimInfo* anim,
                 u32 x, u32 y, u32 w, u32 h,
                 TextIndex text_index) {
    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    Text* text_data = &text_table[(u32)text_index];
    SDL_Point text_size = text_table[(u32)text_index].textSize(false);
    s32 x_os = x + ((w - text_size.x) >> 1);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
      w = PX(1);
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
      w = utils::lerp(PX(1), w, extend_value);
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      drawOrientedText(renderer, text_data, false, x_os, y, text_size.x, text_size.y);
      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
      drawOrientedText(renderer, text_data, false, x_os, y, text_size.x, text_size.y);
    }

    Sint16 vx [] = { x + PX(1), x + w, x + w - PX(1), x };
    Sint16 vy [] = { y,         y,     y + h,         y + h };

    SDL_Color c = renderer->getColor();

    b32 mouse_clicked = mouse->isClicked(x,y,w,h);
    if (mouse_clicked)
      filledPolygonRGBA(renderer->renderer, vx, vy, 4, c.r, c.g, c.b, c.a);
    else
      polygonRGBA(renderer->renderer, vx, vy, 4, c.r, c.g, c.b, c.a);
    return mouse_clicked;
  }

  void drawCheckbox(Renderer* renderer, Tracker* mouse,
                    r32 accumulator, AnimInfo* anim,
                    u32 x, u32 y, b32* enabled) {
    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    u32 w = PX(2);
    u32 h = PX(2);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
      w = 0;
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
      w = utils::lerp(0, w, extend_value);
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
    }

    Sint16 vx [] = { x + PX(1), x + w, x + w - PX(1), x };
    Sint16 vy [] = { y,         y,     y + h,         y + h };

    SDL_Color c = renderer->getColor();

    if (mouse->isClicked(x,y,w,h)) {
      *enabled = !(*enabled);
    }

    if (enabled && *enabled) {
      filledPolygonRGBA(renderer->renderer, vx, vy, 4, c.r, c.g, c.b, c.a);
    } else {
      polygonRGBA(renderer->renderer, vx, vy, 4, c.r, c.g, c.b, c.a);
    }
  }

  b32 drawSelector(Renderer* renderer, Text text_table [], Tracker* mouse,
                   r32 accumulator, AnimInfo* anim,
                   u32 x, u32 y, u32 w, u32 h,
                   TextIndex name_index, TextIndex set_index) {
    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    Text* name_data = &text_table[(u32)name_index];
    Text* set_data  = &text_table[(u32)set_index];

    SDL_Point name_size = name_data->textSize(false);
    SDL_Point set_size  = set_data->textSize(false);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
      w = PX(2);
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
      w = utils::lerp(PX(2), w, extend_value);
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      drawOrientedText(renderer, name_data, false, x + PX(4), y - (name_size.y >> 1), name_size.x, name_size.y);

      b32 is_vert = false;
      if (set_size.x > w) {
        is_vert = true;
        set_size = set_data->textSize(is_vert);
      }
      u32 x_os = ((w - PX(4) - set_size.x) >> 1) + PX(2);
      u32 y_os = ((h - PX(2) - set_size.y) >> 1) + PX(2);
      drawOrientedText(renderer, set_data, is_vert, x + x_os, y + y_os, set_size.x, set_size.y);

      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
      drawOrientedText(renderer, name_data, false, x + PX(4), y - (name_size.y >> 1), name_size.x, name_size.y);

      b32 is_vert = false;
      if (set_size.x > w) {
        is_vert = true;
        set_size = set_data->textSize(is_vert);
      }
      u32 x_os = ((w - PX(4) - set_size.x) >> 1) + PX(2);
      u32 y_os = ((h - PX(2) - set_size.y) >> 1) + PX(2);
      drawOrientedText(renderer, set_data, is_vert, x + x_os, y + y_os, set_size.x, set_size.y);
    }

    SDL_Point points [] = {
      { x + PX(3),               y },
      { x + PX(2),               y },
      { x,                       y + h - PX(2) },
      { x,                       y + h },
      { x + w - PX(2),           y + h },
      { x + w,                   y + h - PX(4) },
      { x + w,                   y },
      { x + PX(5) + name_size.x, y },
    };
    renderer->renderPoly(points, ARRAY_COUNT(points), false);

    SDL_Point extra [] = {
      { x,         y + h },
      { x + PX(2), y + h - PX(4) },
      { x + w,     y + h - PX(4) },
    };
    renderer->renderPoly(extra, ARRAY_COUNT(extra), false);

    return mouse->isClicked(x,y,w,h);
  }

  void drawHorzPanel(Renderer* renderer, Text text_table [],
                     r32 accumulator, AnimInfo* anim,
                     u32 x, u32 y, u32 w, u32 h, TextIndex text_index) {
    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    Text* text_data = &text_table[(u32)text_index];
    SDL_Point text_size = text_data->textSize(false);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
      w = PX(4);
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
      w = utils::lerp(PX(4), w, extend_value);
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      drawOrientedText(renderer, text_data, false, x + PX(5), y - (text_size.y >> 1), text_size.x, text_size.y);
      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
      drawOrientedText(renderer, text_data, false, x + PX(5), y - (text_size.y >> 1), text_size.x, text_size.y);
    }

    SDL_Point left [] = {
      { x + PX(4), y },
      { x + PX(2), y },
      { x,         y + PX(4) },
      { x,         y + h },
      { x + PX(2), y + h },
    };
    renderer->renderPoly(left, ARRAY_COUNT(left), false);

    SDL_Point right [] = {
      { x + w - PX(2), y },
      { x + w,         y },
      { x + w,         y + h - PX(4) },
      { x + w - PX(2), y + h },
      { x + w - PX(4), y + h },
    };
    renderer->renderPoly(right, ARRAY_COUNT(right), false);
  }

  b32 drawLabel(Renderer* renderer, Text text_table [],
                r32 accumulator, AnimInfo* anim,
                u32 x, u32 y, TextIndex text_index,
                Tracker* mouse = nullptr) {
    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    Text* text_data = &text_table[(u32)text_index];
    SDL_Point text_size = text_data->textSize(false);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      drawOrientedText(renderer, text_data, false, x, y, text_size.x, text_size.y);
      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
      drawOrientedText(renderer, text_data, false, x, y, text_size.x, text_size.y);
    }

    return (mouse) ? mouse->isClicked(x, y, text_size.x, text_size.y) : false;
  }

  void drawVertPanel(Renderer* renderer, Text text_table [],
                     r32 accumulator, AnimInfo* anim,
                     u32 x, u32 y, u32 w, u32 h,
                     TextIndex text_index,
                     u32 v_offset = VP_DEFAULT_OFFSET) {
    b32 is_fading = anim->isFading(accumulator);
    r32 fade_value = anim->getFadeValue(accumulator);
    b32 is_extending = anim->isExtending(accumulator);
    r32 extend_value = anim->getExtendValue(accumulator);
    b32 is_text_fading = anim->isTextFading(accumulator);
    r32 text_fade_value = anim->getTextFadeValue(accumulator);

    Text* text_data = &text_table[(u32)text_index];
    SDL_Point text_size = text_data->textSize(false);

    if (is_fading) {
      renderer->setAlpha(color, fade_value);
      update_ui = true;
      h = PX(8);
    } else if (is_extending) {
      renderer->setColor(color);
      update_ui = true;
      h = utils::lerp(PX(8), h, extend_value);
    } else if (is_text_fading) {
      renderer->setAlpha(color, fade_value);
      drawOrientedText(renderer, text_data, false, x + PX(5), y - (text_size.y >> 1), text_size.x, text_size.y);
      renderer->setColor(color);
      update_ui = true;
    } else {
      renderer->setColor(color);
      drawOrientedText(renderer, text_data, false, x + PX(5), y - (text_size.y >> 1), text_size.x, text_size.y);
    }

    SDL_Point top0 [] = {
      { x,         y + PX(v_offset+6) },
      { x,         y + PX(4) },
      { x + PX(2), y },
      { x + PX(4), y },
    };
    renderer->renderPoly(top0, ARRAY_COUNT(top0), false);

    SDL_Point top1 [] = {
      { x + PX(7) + text_size.x, y },
      { x + w,                   y },
      { x + w,                   y + PX(v_offset+2) },
    };
    renderer->renderPoly(top1, ARRAY_COUNT(top1), false);

    SDL_Point middle [] = {
      { x,         y + PX(v_offset+4) },
      { x + PX(2), y + PX(v_offset) },
      { x + w,     y + PX(v_offset) },
    };
    renderer->renderPoly(middle, ARRAY_COUNT(middle), false);

    SDL_Point bottom [] = {
      { x,             y + h - PX(2)},
      { x,             y + h },
      { x + w - PX(2), y + h },
      { x + w,         y + h - PX(4) },
      { x + w,         y + h - PX(6) },
    };
    renderer->renderPoly(bottom, ARRAY_COUNT(bottom), false);
  }

  inline
  void drawTextEditWithLayout(Renderer* renderer, Text* text, Tracker* mouse,
                              r32 accumulator, AnimInfo* anim,
                              Layout* layout, u32 offset, u32 pw, u32 ph,
                              String* user_text, b32 enabled,
                              void(* click_func)(String*) = nullptr) {
    drawTextEdit(renderer, text, mouse, accumulator, anim, PX(layout->x), PX(layout->y), PX(pw), PX(ph), user_text, enabled, click_func);
    layout->x += pw + offset;
  }

  inline
  b32 drawButtonWithLayout(Renderer* renderer, Text text_table [], Tracker* mouse,
                           r32 accumulator, AnimInfo* anim,
                           Layout* layout, u32 offset, u32 pw, u32 ph,
                           TextIndex text_index) {
    b32 result = drawButton(renderer, text_table, mouse, accumulator, anim, PX(layout->x), PX(layout->y), PX(pw), PX(ph), text_index);
    layout->x += pw + offset;
    return result;
  }

  inline
  void drawCheckboxWithLayout(Renderer* renderer, Tracker* mouse,
                              r32 accumulator, AnimInfo* anim,
                              Layout* layout, u32 offset, b32* enabled) {
    drawCheckbox(renderer, mouse, accumulator, anim, PX(layout->x), PX(layout->y), enabled);
    layout->x += 2 + offset;
  }

  inline
  b32 drawSelectorWithLayout(Renderer* renderer, Text text_table [], Tracker* mouse,
                             r32 accumulator, AnimInfo* anim, Layout* layout,
                             u32 pw, u32 ph, TextIndex name_index, TextIndex set_index) {
    b32 result = drawSelector(renderer, text_table, mouse, accumulator, anim, PX(layout->x), PX(layout->y), PX(pw), PX(ph), name_index, set_index);
    layout->x += pw;
    return result;
  }

  inline
  b32 drawLabelWithLayout(Renderer* renderer, Text text_table [],
                          r32 accumulator, AnimInfo* anim,
                          Layout* layout, u32 offset, TextIndex text_index, Tracker* mouse = nullptr) {
    b32 result = drawLabel(renderer, text_table, accumulator, anim, PX(layout->x), PX(layout->y), text_index, mouse);
    u32 w = text_table[(u32)text_index].textSize(false).x;
    layout->x += (w / TILE_SIZE + 1) + offset;
    return result;
  }
}